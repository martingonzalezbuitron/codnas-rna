"""\
Purpose: With this function you can remove files or dirs if exits.

Preconditions:
                - Files for input
                    * nothing
                - Libraries
                    * nothing

Arguments:    -Positional
                * [alist]           // list

Observations:
            - 

TODO: - 

"""

import argparse
import os
import shutil
from build_ok_outputDir_string import build_ok_outputDir_string


def remove_if_exist(alist):
    """
    Remove file/s or dir/s if exits.
    Parameters:
                    * alist: a list of strings.
                             Denote files or dirs.
    Returns: nothing.
    """
    for element in alist:
        if os.path.isfile(element):
            os.remove(element)
        else:
            element = build_ok_outputDir_string(element)
            if os.path.isdir(element):
                shutil.rmtree(element)
    return


if __name__ == "__main__":

    p = argparse.ArgumentParser(description="Remove file/s or dir/s if exits.")
    # Positional arguments
    p.add_argument(
        "alist",
        help="List of paths to files and/or directories."
    )

    args = p.parse_args()
    remove_if_exist(**vars(args))

# Written by Martín González Buitrón
# Email: martingonzalezbuitron@gmail.com