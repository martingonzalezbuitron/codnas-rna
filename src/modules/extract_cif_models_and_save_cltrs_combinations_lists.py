"""\
Purpose: With this module you can extract models.cif and create
         cluster combination lists using *.cif files.
         If some chain name has not an acceptable PDB format length,
         is renamed as \"?\" in the model and that chain is saved
         in \"renamed_chains_for_aligment.list\".

Preconditions:
                - Files for input
                    * cif files
                - Libraries
                    * Pandas
                    * gemmi

Arguments:    -Positional
                * [clusters_csv_list]              // list
                * [cif_dir]                        // string
                * [outputDir]                      // string

Observations: -

TODO: - 

"""

import argparse
import os
import pandas as pd
import gemmi
from itertools import combinations
from build_ok_outputDir_string import build_ok_outputDir_string
from remove_if_exist import remove_if_exist


def get_dict_pdb_chains(clusters_csv_list):
    """
    Get a dict of PDBs keys with list of tuplas "(CHAIN, clstr_ID)".
    Parameters:
                    * clusters_csv_list:        a list of each cluster path.
    Return: a dict. {PDB:[(CHAIN, clstr_ID),(CHAIN, clstr_ID)]}.
    """
    #Create a copy to process
    clusters_csv_list_to_see = clusters_csv_list.copy()
    #Create empty dict
    dict_pdb_chains = dict()
    # Start to process each cluster
    while len(clusters_csv_list_to_see) != 0:
        file = clusters_csv_list_to_see.pop(0)
        cluster = pd.read_csv(file, dtype=str, header=0)
        cluster_id = os.path.basename(file).strip(".csv")
        for pdb_chain in cluster['Seq_code'].to_list():
            pdb = pdb_chain.split(':')[0]
            chain = pdb_chain.split(':')[1]
            if pdb in dict_pdb_chains.keys():
                if not chain in dict_pdb_chains[pdb]:
                    dict_pdb_chains[pdb].append((chain, cluster_id))
            else:
                dict_pdb_chains[pdb] = [(chain, cluster_id)]
    return dict_pdb_chains


def get_structure_cif_file(pdb_code, cif_dir):
    """
    Get a stucture file.
    Parameters:
                    * pdb_code:                 a string. Correspond to PDB code.
                    * cif_dir:                  a string. Correspond to path directory
                                                          where all cif files are stored.
    Return: a file of one PDB structure.
    """
    #Always lower case due downloaded RCSB name files
    pdb_code = pdb_code.lower()
    #First check if *.cif file exists
    if not os.path.exists(cif_dir + pdb_code + ".cif"):
        raise FileNotFoundError('[Error] File not exists: {}.cif'.format(cif_dir + pdb_code))
    #Open the file (not need it for gemmi)
#    file = open(cif_dir + pdb_code + ".cif", "r")
    #Get the corresponding structure
    structure_file = gemmi.read_structure(cif_dir + pdb_code + ".cif")
    return structure_file


def save_cif_models_chains_and_get_dict_cltr_chains(dict_pdb_chains, cif_dir):
    """
    Save extracted model chains using *.cif files located in cif_dir/ and
    get a dict of clusters with every PDB_MODEL_CHAIN.cif.
    Parameters:
                    * dict_pdb_chains:          a dict of PDB:Chains.
                    * cif_dir:                  a string. Correspond to path directory
                                                          where all cif files are stored.
    Return: a dict. {Cluster_ID:[PDB_MODEL_CHAIN.cif, PDB_MODEL_CHAIN.cif]}.
    """
    #Directory to save PDB_MODEL_CHAIN.cif files
    cif_dir = build_ok_outputDir_string(cif_dir)
    parent_cif_dir = os.path.dirname(os.path.abspath(cif_dir))
    data_codnas_rna_model_chains_dir = parent_cif_dir + "/" + "data_codnas_rna_model_chains"
    data_codnas_rna_model_chains_dir = build_ok_outputDir_string(data_codnas_rna_model_chains_dir)
    # Make dir or do nothing if exists
    os.makedirs(data_codnas_rna_model_chains_dir, exist_ok=True)

    dict_cltr_chains = dict()
    dict_renamed_cltr_chains = dict()
    for pdb_code in dict_pdb_chains.keys():
        #Open and load mmcif file
        mmcif_file = get_structure_cif_file(pdb_code, cif_dir)
        #Iterate throght models in mmcif_file
        for model in mmcif_file:
            for chain_cltrID in dict_pdb_chains[pdb_code]:
                chain = chain_cltrID[0]
                cluster_id = chain_cltrID[1]
                model_filename = (pdb_code.lower() + "_" + str(model.name) + "_" + chain + ".pdb")
                output_model_filename = data_codnas_rna_model_chains_dir + model_filename
                #First check if file already exists
                if not os.path.exists(output_model_filename):
                    #Start a new Structure backbone. Use gemmi Structure() constructor
                    st = gemmi.Structure()
                    #Give a name to that new Structure
                    st.name = pdb_code
                    #Create a Model to put in Structure
                    st.add_model(gemmi.Model(model.name))

                    #Select a chain from a model in mmcif_file
                    mmcif_chain = mmcif_file[model.name][chain]

                    #Get the only model present in Structure
                    st_model = st[0]
                    #Put a chain inside the model to be saved
                    st_model.add_chain(mmcif_chain)
                    if len(st_model[0].name) > 1:
                        #Too long for PDBx format
                        st_model[0].name = '?'
                        if cluster_id in dict_renamed_cltr_chains.keys():
                            #Check if model_filename already exists in corresponding cluster ID
                            if not model_filename in dict_renamed_cltr_chains[cluster_id]:
                                #Save model_filename entry
                                dict_renamed_cltr_chains[cluster_id].append(model_filename)
                        else:
                            #Save cluster_ID:[model_filename] entry in dict_cltr_chains
                            dict_renamed_cltr_chains[cluster_id] = [model_filename]
                    #Save PDB-MODEL-CHAIN.pdb
                    st.write_pdb(output_model_filename)
                else:
                    print(model_filename+" already exists in "+data_codnas_rna_model_chains_dir+" .")
                #Check if that cluster ID exist in dict_cltr_chain()
                if cluster_id in dict_cltr_chains.keys():
                    #Check if model_filename already exists in corresponding cluster ID
                    if not model_filename in dict_cltr_chains[cluster_id]:
                        #Save model_filename entry
                        dict_cltr_chains[cluster_id].append(model_filename)
                else:
                    #Save cluster_ID:[model_filename] entry in dict_cltr_chains
                    dict_cltr_chains[cluster_id] = [model_filename]
    return dict_cltr_chains, dict_renamed_cltr_chains


def save_cltr_file_combinations(chain_combinations, cluster_id, outputDirComb):
    """
    Save cluster combination list in outputDir.
    Parameters:
                    * chain_combinations:       a list of PDB MODEL CHAIN combinations.
                    * cluster_id:               a string. Correspond to cluster id.
                    * outputDirComb:            a string. Directory to save
                                                          combination list files.
    Returns: nothing.
    """
    # Save each cluster pdb_code + model_chain combination results
    with open(outputDirComb + cluster_id + "_comb.list", "a") as outfile:
        for comb in chain_combinations:
            outfile.write("%s %s\n" % (comb[0], comb[1]))
    return


def perform_and_save_cltrs_combination_files(dict_cltr_chains, outputDir):
    """
    Perform and save cluster chains combinations of all PDB_MODEL_CHAINs.cif.
    Parameters:
                    * dict_cltr_chains:         a dict of cltrs with PDB:Chains.
                    * outputDir:                a string. Directory to save
                                                          "Clusters_combinations/".
    Return: nothing.
    """
    #Directory to save combinations files
    outputDir = build_ok_outputDir_string(outputDir)
    # Define output file
    outputDirComb = outputDir + "Clusters_combinations/"
    # Check and build outputDir correctly
    outputDirComb = build_ok_outputDir_string(outputDirComb)
    # Remove this directory first so everything is prepared for every database running test
    remove_if_exist([outputDirComb])
    # Make dir or do nothing if exists
    os.makedirs(outputDirComb, exist_ok=True)
    #Start to perform chain combinations on each cluster and save it
    for cluster_id, model_chain_list in dict_cltr_chains.items():
        chain_combinations = {comb for comb in combinations(model_chain_list, 2)}
        save_cltr_file_combinations(chain_combinations, cluster_id, outputDirComb)
    return


def save_renamed_chains(dict_renamed_cltr_chains, outputDir):
    """
    Save renamed chains saved as PDB_MODEL_CHAINs.cif.
    Parameters:
                    * dict_renamed_cltr_chains: a dict of cltrs with PDB:Chains.
                    * outputDir:                a string. Directory to save
                                                          combination list files.
    Return: nothing.
    """
    #Directory to save a list with with all renamed chains
    outputDir = build_ok_outputDir_string(outputDir)
    # Make dir or do nothing if exists
    os.makedirs(outputDir, exist_ok=True)
    # Save list
    with open(outputDir + "renamed_chains_for_aligment.list", "a") as outfile:
        for cluster_id, model_chain_list in dict_renamed_cltr_chains.items():
            for chain in model_chain_list:
                outfile.write("%s,%s\n" % (cluster_id,chain))
    return


def extract_cif_models_and_save_cltrs_combinations_lists(clusters_csv_list, cif_dir, outputDir):
    """
    Extract every chain model from every corresponding cluster and create
    every cluster combination list. All of this is performed using *.cif
    files located in cif_dir/. Structural *.cif models are saved under the
    following folder (data_codnas_rna_model_chains/) in same directory as
    cif_dir/ is contained. Combination lists files are saved in outputDir
    under the following folder (Clusters_combinations/).
    Parameters:
                    * clusters_csv_list:        a list of each cluster path.
                    * cif_dir:                  a string. Correspond to path directory
                                                          where all cif files are stored.
                    * outputDir:                a string. Directory to save Clusters_combinations/.
    Returns: nothing.
    """
    dict_pdb_chains = get_dict_pdb_chains(clusters_csv_list)
    dict_cltr_chains, dict_renamed_cltr_chains = save_cif_models_chains_and_get_dict_cltr_chains(dict_pdb_chains, cif_dir)
    perform_and_save_cltrs_combination_files(dict_cltr_chains, outputDir)
    save_renamed_chains(dict_renamed_cltr_chains, outputDir)

    return


if __name__ == "__main__":

    p = argparse.ArgumentParser(
        description="Extract models.cif and create cluster combination lists using *.cif files.\nIf some chain name has not an acceptable PDB format length, is renamed as \"?\" in the model and\nthat chain is saved in \"renamed_chains_for_aligment.list\"."
    )
    # Positional arguments
    p.add_argument(
        "clusters_csv_list",
        help="List of clusters *.csv paths."
    )
    p.add_argument(
        "cif_dir",
        type=str,
        help="Output directory to search *.cif files and used to search a parent directory\nwhere to create data_codnas_rna_model_chains/ folder and save all *.pdb models.	"
    )
    p.add_argument(
        "outputDir",
        type=str,
        help="Output directory to create Clusters_combinations/ folder and save all cluster combination lists."
    )

    args = p.parse_args()
    extract_cif_models_and_save_cltrs_combinations_lists(**vars(args))

# Written by Martín González Buitrón
# Email: martingonzalezbuitron@gmail.com