"""\
Purpose: With this module you can create a set of CoDNaS-RNA
         cdrna_cluster_<auxiliar>.csv.gz website tables.

Preconditions:
                - Files for input
                    * cdrna_rnacentral
                    * cdrna_conformers
                    * pdbchains_databases_codes
                - Libraries
                    * Pandas
                    * Numpy

Arguments:    -Positional
                * [cdrna_rnacentral]             // string
                * [cdrna_conformers]             // string
                * [pdbchains_databases_codes]    // string
                * [outputDir]                    // string

Observations:
            - 

TODO: - 

"""

import argparse
import os
import pandas as pd
from build_cdrna_clusters import get_urs_values, get_column_values
from add_cell_value import add_value_in_column_to_subset
from save_website_table import save_website_table
from get_website_df import get_df_if_file_exist
from create_dir import create_dir
from build_ok_outputDir_string import build_ok_outputDir_string


def save_cltr_rnatype_table(cdrna_rnacentral, cdrna_conformers, outputDir):
    """
    Create and save auxiliar cluster:rna_type table to be used in search option.
    If is not present in RNAcentral, likes mRNAs, rna types are taken from cdrna_conformers
    dataframe.
    Parameters:
                * cdrna_rnacentral:          a dataframe. Correspond to a pandas dataframe for
                                                          CoDNaS-RNA rnacentral website table.
                * cdrna_conformers:          a dataframe. Correspond to a pandas dataframe for
                                                          CoDNaS-RNA conformers website table.
                * outputDir:                 a string. Correspond to an output directory to store
                                                       CoDNaS-RNA website tables.
    Return: nothing
    """
    # Get new dataframe with only two columns
    cltr_rna_type = cdrna_rnacentral[["cluster_id", "rna_type"]].copy()
    # Remove duplicate rows
    cltr_rna_type = cltr_rna_type.drop_duplicates(
        subset=["cluster_id", "rna_type"]
    ).copy()
    # Create a list of all clusters that are present
    cltr_present = cltr_rna_type["cluster_id"].unique().tolist()

    cltrs_others_types = cdrna_conformers[["cluster_id", "macromolecule"]].copy()
    # Rename columns
    cltrs_others_types = cltrs_others_types.rename(
        columns={"macromolecule": "rna_type"}
    )
    # Remove duplicate rows
    cltrs_others_types = cltrs_others_types.drop_duplicates().copy()
    # Get only clusters that are no present in RNAcentral already
    cltrs_others_types = cltrs_others_types[
        ~cltrs_others_types["cluster_id"].isin(cltr_present)
    ].copy()

    # Start to iterate one by one to see the RNA type. Classify if it is possible.
    for index, row in cltrs_others_types.iterrows():
        cltr_id = row["cluster_id"]
        atype = row["rna_type"].lower()
        if "mrna" in atype:
            atype = "mRNA"
        elif "messenger rna" in atype:
            atype = "mRNA"
        elif "rrna" in atype:
            atype = "rRNA"
        elif "ribosomal rna" in atype:
            atype = "rRNA"
        elif "trna" in atype:
            atype = "tRNA"
        else:
            atype = "unknown"
        # Add clusters with custom RNA type to cltr_rna_type dataframe
        cltr_rna_type = cltr_rna_type.append(
            {"cluster_id": cltr_id, "rna_type": atype}, ignore_index=True, sort=False
        )
    # Re-check duplicated for renamed other types
    cltr_rna_type = cltr_rna_type.drop_duplicates().copy()

    # Add percentage in each RNA type per cluster.
    for cltr_id in cltr_rna_type["cluster_id"].unique().tolist():
        # Get name values from cluster
        rna_type_main, rna_type_list = get_column_values(
            cltr_id, cdrna_rnacentral, "rna_type"
        )

        # Unique list of rna_type
        rna_type_list = rna_type_main + rna_type_list

        #Remove NA from list
        rna_type_list = [a_tuple for a_tuple in rna_type_list if a_tuple != "NA"]

        # Add RNA type values to auxiliar table
        for a_tuple in rna_type_list:
            a_rna_type, per = a_tuple[0], a_tuple[1]
            cltr_rna_type.loc[
                                (cltr_rna_type["cluster_id"] == cltr_id) &
                                (cltr_rna_type["rna_type"] == a_rna_type)
                            ,
                            "per_rna_type"
                            ] = per

    # Save dataframe as a csv.gz file.
    save_website_table(cltr_rna_type, outputDir, "cdrna_cluster_rnatype", ",")

    return


def save_cltr_specie_table(cdrna_rnacentral, outputDir):
    """
    Create and save auxiliar cluster:specie table to be used in search option.
    Parameters:
                * cdrna_rnacentral:          a dataframe. Correspond to a pandas dataframe for
                                                          CoDNaS-RNA rnacentral website table.
                * outputDir:                 a string. Correspond to an output directory to store
                                                       CoDNaS-RNA website tables.
    Return: nothing
    """
    # Get new dataframe with only two columns
    cltr_specie = cdrna_rnacentral[["cluster_id", "specie"]].copy()
    # Remove duplicate rows
    cltr_specie = cltr_specie.drop_duplicates(subset=["cluster_id", "specie"]).copy()


    # Add percentage in each URS ID per cluster.
    for cltr_id in cltr_specie["cluster_id"].unique().tolist():
        # Get source values from cluster
        specie_main, specie_list = get_column_values(
            cltr_id, cdrna_rnacentral, "specie"
        )

        # Unique list of species
        specie_list = specie_main + specie_list

        # Add Specie values to auxiliar table
        for a_tuple in specie_list:
            if a_tuple is "NA":
                continue
            a_specie, per = a_tuple[0], a_tuple[1]
            cltr_specie.loc[
                                (cltr_specie["cluster_id"] == cltr_id) &
                                (cltr_specie["specie"] == a_specie)
                            ,
                            "per_specie"
                            ] = per

    # Save dataframe as a csv.gz file.
    save_website_table(cltr_specie, outputDir, "cdrna_cluster_specie", ",")

    return


def save_cltr_ursid_table(pdbchains_databases_codes, outputDir):
    """
    Create and save auxiliar cluster:urs_id table to be used in search option.
    Parameters:
                * pdbchains_databases_codes: a string. Correspond to a TSV table with
                                                       all PDB values and releated codes.
                * outputDir:                 a string. Correspond to an output directory to store
                                                       CoDNaS-RNA website tables.
    Return: nothing
    """
    ###############################################################################################################################
    # Until postgres it is fixed this option has less cluster:urs combinations (1076 instead of 1079)

    # Get new dataframe with only two columns
    #    cltr_urs_id = cdrna_rnacentral[['cluster_id', 'urs_id']].copy()
    # Remove duplicate rows
    #    cltr_urs_id = cltr_urs_id.drop_duplicates(subset=['cluster_id', 'urs_id']).copy()
    ###############################################################################################################################
    # Get dataframe to work with
    mapping_table = get_df_if_file_exist(
        "mapping_table", pdbchains_databases_codes, "\t", False
    )
    # Get clean dataframe (without NA and RNAcentral id not found).
    mapping_table_clean = mapping_table.loc[
        (mapping_table["Cluster_ID"] != "NA")
        & (mapping_table["RNAcentral_URS"] != "RNAcentral id not found")
    ].copy()
    # Get new dataframe with only two columns
    cltr_urs_id = mapping_table_clean[["Cluster_ID", "RNAcentral_URS"]].copy()
    # Rename columns
    cltr_urs_id = cltr_urs_id.rename(
        columns={"Cluster_ID": "cluster_id", "RNAcentral_URS": "urs_id"}
    )
    # Remove duplicates
    cltr_urs_id = cltr_urs_id.drop_duplicates(subset=["cluster_id", "urs_id"]).copy()


    # Add percentage in each URS ID per cluster.
    for cltr_id in mapping_table["Cluster_ID"].dropna().unique().tolist():
        # Get URS values from cluster
        urs_main, urs_list = get_urs_values(cltr_id, mapping_table)

        # Unique list of URS IDs
        urs_list = urs_main + urs_list

        # Add URS values to auxiliar table
        for a_tuple in urs_list:
            if a_tuple is "NA":
                continue
            a_urs, per = a_tuple[0], a_tuple[1]
            add_value_in_column_to_subset(
                cltr_urs_id,
                a_urs,
                "urs_id",
                per,
                "per_urs_id"
            )

    # Save dataframe as a csv.gz file.
    save_website_table(cltr_urs_id, outputDir, "cdrna_cluster_ursid", ",")

    return


def save_NA_tables(pdbchains_databases_codes, outputDir):
    """
    Create and save auxiliar "urs_id" and "pdb,chain" NA tables to be used as second option
    in search filed.
    Parameters:
                * pdbchains_databases_codes: a string. Correspond to a TSV table with
                                                       all PDB values and releated codes.
                * outputDir:                 a string. Correspond to an output directory to store
                                                       CoDNaS-RNA website tables.
    Return: nothing
    """
    ###############################################################################################################################
    # Until postgres it is fixed this option has less cluster:urs combinations (1076 instead of 1079)
    #
    # Get new dataframe with only two columns
    #    cltr_urs_id = cdrna_rnacentral[['cluster_id', 'urs_id']].copy()
    # Remove duplicate rows
    #    cltr_urs_id = cltr_urs_id.drop_duplicates(subset=['cluster_id', 'urs_id']).copy()
    ###############################################################################################################################
    # Get dataframe to work with
    mapping_table = get_df_if_file_exist(
        "mapping_table", pdbchains_databases_codes, "\t", False
    )
    # Get clean dataframe (without NA and RNAcentral id not found).
    mapping_table_clean = mapping_table.loc[
        (mapping_table["Cluster_ID"] == "NA")
    ].copy()

    # Get new dataframes with only one column
    # PDB_chain
    na_pdb_chain = mapping_table_clean[["PDB_chain"]].copy()
    # URS IDs
    na_urs_id = mapping_table_clean[["RNAcentral_URS"]].copy()

    # Split PDB_chain column
    na_pdb_chain["pdb"], na_pdb_chain["chain"] = na_pdb_chain["PDB_chain"].str.split(":", 1).str
    # Remove PDB_chain
    na_pdb_chain = na_pdb_chain.drop(["PDB_chain"], axis=1)

    # Rename column
    na_urs_id = na_urs_id.rename(
        columns={"RNAcentral_URS": "urs_id"}
    )
    # Remove duplicates
    na_urs_id = na_urs_id.drop_duplicates(subset=["urs_id"]).copy()

    # Remove RNAcentral id not found from table
    na_urs_id = na_urs_id.loc[
        (na_urs_id["urs_id"] != "RNAcentral id not found")
    ]

    # Save dataframe as a csv.gz file.
    save_website_table(na_pdb_chain, outputDir, "cdrna_na_pdb_chain", ",")
    # Save dataframe as a csv.gz file.
    save_website_table(na_urs_id, outputDir, "cdrna_na_ursid", ",")

    return


def create_auxiliar_website_tables(
    cdrna_rnacentral,
    cdrna_conformers,
    pdbchains_databases_codes,
    outputDir
):
    """
    Create auxiliar website tables to be used in search option.
    Parameters:
                * cdrna_rnacentral:          a dataframe. Correspond to a pandas dataframe for
                                                          CoDNaS-RNA rnacentral website table.
                * cdrna_conformers:          a string. Correspond to a CSV CoDNaS-RNA conformers
                                                       website table.
                * pdbchains_databases_codes: a string. Correspond to a TSV table with
                                                       all PDB values and releated codes.
                * outputDir:                 a string. Correspond to an output directory to store
                                                       CoDNaS-RNA website tables.
    Return: nothing
    """
    # Before start to do anything, check if outputDir is correct and create it
    outputDir = build_ok_outputDir_string(outputDir)
    create_dir(outputDir)

    save_NA_tables(pdbchains_databases_codes, outputDir)

    # This is a FLAG to be faster to debug
    if (
        (os.path.exists(outputDir + "cdrna_cluster_rnatype.csv.gz"))
        and (os.path.exists(outputDir + "cdrna_cluster_specie.csv.gz"))
        and (os.path.exists(outputDir + "cdrna_cluster_ursid.csv.gz"))
    ):
        return

    # Get dataframe to work with
    cdrna_rnacentral = get_df_if_file_exist(
        "cdrna_rnacentral", cdrna_rnacentral, ",", False
    )

    # Get dataframe to work with
    cdrna_conformers = get_df_if_file_exist(
        "cdrna_conformers", cdrna_conformers, ",", False
    )

    # Don't count unassigned clusters
    cdrna_conformers = cdrna_conformers.loc[
        (cdrna_conformers["cluster_id"] != "NA")
    ].copy()

    # Save auxiliar tables
    save_cltr_ursid_table(pdbchains_databases_codes, outputDir)
    save_cltr_rnatype_table(cdrna_rnacentral, cdrna_conformers, outputDir)
    save_cltr_specie_table(cdrna_rnacentral, outputDir)

    print("All done !")

    return


# cdrna_rnacentral = '/home/martingb/Projects/2020/CoDNaS-RNA/results/website/releases/2021-09/tables/cdrna_rnacentral.csv.gz'
# cdrna_conformers = '/home/martingb/Projects/2020/CoDNaS-RNA/results/website/releases/2021-09/tables/cdrna_conformers.csv.gz'
# pdbchains_databases_codes = '/home/martingb/Projects/2020/CoDNaS-RNA/processed_data/2020-05-15/tables/PDBchains_with_databases_codes.tsv'
# outputDir = '/home/martingb/Projects/2020/CoDNaS-RNA/results/website/releases/2021-09/tables/'


if __name__ == "__main__":

    p = argparse.ArgumentParser(
        description="Create cdrna_cluster_<auxiliar>.csv.gz table to be used in CoDNaS-RNA website."
    )
    # Positional arguments
    p.add_argument(
        "cdrna_rnacentral",
        type=str,
        help="CoDNaS-RNA rnacentral website table."
    )
    p.add_argument(
        "cdrna_conformers",
        type=str,
        help="Table with all CoDNaS-RNA conformers from website."
    )
    p.add_argument(
        "pdbchains_databases_codes",
        type=str,
        help="Table with all PDB chains with their related codes."
    )
    p.add_argument(
        "outputDir",
        type=str,
        help="Path where to store cdrna_rnacentral table."
    )

    args = p.parse_args()
    create_auxiliar_website_tables(**vars(args))

# Written by Martín González Buitrón
# Email: martingonzalezbuitron@gmail.com