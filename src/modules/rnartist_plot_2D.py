"""\
Purpose: With this module you can plot vienna files with RNArtistCore and create 2D SVG pictures.
         This module will create a kotlin folder (ktn/) in the output directory with all respective
         kotlin files mantaining same name as each vienna file has.
         For plotting results, each file is going to be stored under a folder called ss_2D/
         in the output directory.

Preconditions:
                - Files for input
                    * dbn/                      // vienna files (*.rev.dbn for CoDNaS-RNA)
                - Libraries
                    * Pandas
                - Software
                    * rnartistcore-0.2.7

Arguments:    -Positional
                * [outputDir]                      // string

Observations:
            - Current SS formats supported:
                * vienna (default outputs are like <input-filename>.<vienna>)

TODO: - 

"""


import argparse
import os
import glob
from get_filename import get_filename
from create_dir import create_dir
from build_ok_outputDir_string import build_ok_outputDir_string
from vienna import SSFormat


def get_list_of_files(aDir, extformat):
    """
    Get a list of absolute paths for all "extformat" files present in "aDir".
    Parameters:
                * aDir:                     a string. Correspond to a directory path where all
                                                      extformat files are stored.
                * extformat:                a string. Correspond to format extention (e.g.: tsv).
    Return: a list
    """
    if os.path.exists(aDir):
        #Get data
        file_list = glob.glob(aDir + '*.' + extformat + "*")
        if not file_list:
            raise ValueError('[Error] No files ended with {} were found on {}.'.format('.'+extformat, aDir))
    else:
        raise NameError('[Error] Directory not exists: {}.'.format(aDir))
    return file_list


def create_kotlin_file(kts_string, filename, outputDir):
    """
    Create kotlin file (*.kts) to be used as input of RNArtist.
    Its also creates a ktn/ folder where to save the file.
    Parameters:
                * kts_string:                 a string. Correspond to a full kotlin string.
                * filename:                   a string. Correspond to a file name.
                * outputDir:                  a string. Directory to save the ktn/ folder.
    Return: a string
    """
    outputDir = outputDir + "ktn/"
    if not os.path.exists(outputDir):
        create_dir(outputDir)

    kts_filename = outputDir + filename + ".kts"
    if not os.path.exists(kts_filename):
        with open(kts_filename, "w") as afile:
            afile.write(kts_string + "\n")

    return kts_filename


def choose_color_schemes():
    """
    Choose a color schema of RNArtist.
    Parameters: nothing
    Return: a string
    """
    persian_carolina = '''
        color {
          type="A"
          value = "#d741a7"
        }

        color {
          type="c"
          value = "black"
        }

        color {
          type="U"
          value = "#3a1772"
        }

        color {
          type="G"
          value = "#5398be"
        }

        color {
          type="C"
          value = "#f2cd5d"
        }'''

    return persian_carolina


def create_kotlin_string(SS_file):
    """
    Create kotlin string to be used as input of RNArtist.
    Parameters:
                * SS_file:                 a file. Correspond to a vienna fasta-like file.
    Return: a string
    """
    filename = SS_file.name + ".svg"  # RNArtistCore do weird stuff with out output name
    color_scheme = choose_color_schemes() # One option for now

    kts_string = '''rnartist {
      file = "'''+filename+'''"
      ss {
          rna {
            sequence = "'''+SS_file.sequence+'''"
          }
          bracket_notation = "'''+SS_file.sstr+'''"
      }
      theme {
        details_lvl = 5

        details {
          type="tertiary_interaction"
          value = "full"
        }
        '''+color_scheme+'''
      }
    }
    '''
    return kts_string


def fix_svg_files():
    """
    Rename svg output filenames of RNArtist.
    Its takes all svg files under current directory and move them
    to a new directory (ss_2D/).
    Parameters: nothing
    Return: nothing
    """
    prov_outputDir = os.path.expanduser(os.getcwd()+"/ss_2D/")
    if not os.path.exists(prov_outputDir):
        create_dir(prov_outputDir)

    svg_files = get_list_of_files(os.getcwd() + "/", "svg")
    for svg_input in svg_files:
        filename = get_filename(svg_input, without_exts=True)
        filename = "_".join(filename.split("_")[0:3])
        svg_output = prov_outputDir + filename + ".svg"
        cmd_move_svg = "mv " + svg_input + " " + svg_output
        os.system(cmd_move_svg)
    return


def move_ss_2D(outputDir):
    """
    Moves ss_2D/ folder to output directory.
    Parameters:
                * outputDir:                  a string. Directory to move the ss_2D/ folder.
    Return: nothing
    """
    if not os.path.exists(outputDir):
        create_dir(outputDir)
    prov_outputDir = os.path.expanduser(os.getcwd()+"/ss_2D")
    cmd_move_ss_2D = "mv " + prov_outputDir + " " + outputDir
    os.system(cmd_move_ss_2D)
    return


def create_files( viennaDir
                , rnartistcore_jar
                , outputDir ):
    """
    Generate 2D plots using RNArtistCore. Files created are stored in two folders, ktn/
    and rnartist_2D/.
    Parameters:
                * viennaDir:                 a string. Correspond to an output directory where are
                                                       stored the vienna dbn files.
                * rnartistcore_jar:          a string. Correspond to an absolute path of
                                                       rnartistcore executable java file.
                * outputDir:                 a string. Correspond to an output directory to store
                                                       the ktn/ and ss_2D/ files.
    Return: nothing
    """
    outputDir = build_ok_outputDir_string(outputDir)
    vienna_files_list = get_list_of_files(viennaDir, "vienna")

#    cltrs_list = ["529","851", "7", "733", "516", "846", "726", "410", "812", "915", "737", "499", "952", "12"]

#    pdb_chain_dict = get_pdb_chain_dict(pdbchains_databases_codes, a_separator)

    for vienna_file in vienna_files_list:
        # Load CoDNaS-RNA's vienna file (like a fasta file)
        SS_file = SSFormat(vienna_file)
        kts_string = create_kotlin_string(SS_file)
        kts_filename = create_kotlin_file(kts_string, SS_file.name, outputDir)
        rnartistcore = "java -jar " + rnartistcore_jar + " "
        cmd_rnartist = rnartistcore + kts_filename + " > /dev/null 2>&1"
        os.system(cmd_rnartist)
    fix_svg_files()
    move_ss_2D(outputDir)
    print("All done !")

    return


# viennaDir = '/home/martingb/Projects/2020/CoDNaS-RNA/processed_data/2020-05-15/dbns/vienna_broken_clean/'
# outputDir = '/home/martingb/Projects/2020/CoDNaS-RNA/processed_data/2020-05-15/tables/'
# rnartistcore = 'rnartistcore-0.2.7-SNAPSHOT-jar-with-dependencies.jar'


if __name__ == "__main__":

    p = argparse.ArgumentParser(
        description="Create 2D plots using RNArtistCore and vienna dot-bracket-notation files."
    )
    # Positional arguments
    p.add_argument(
        "viennaDir",
        type=str,
        help="Path to grab vienna dbn files."
    )
    p.add_argument(
        "rnartistcore_jar",
        type=str,
        help="Absolute path to RNArtistCore java executable (rnartistcore-{version}-SNAPSHOT-jar-with-dependencies.jar)."
    )
    p.add_argument(
        "outputDir",
        type=str,
        help="Path where to store kotlin (ktn/*.kts) and SVG files (ss_2D/*.svg)."
    )

    args = p.parse_args()
    create_files(**vars(args))

# Written by Martín González Buitrón
# Email: martingonzalezbuitron@gmail.com