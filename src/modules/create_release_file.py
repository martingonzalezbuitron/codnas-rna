"""\
Purpose: With this module you can create a release_20XX-XX.json file
         to be used in CoDNaS-RNA website.

Preconditions:
                - Files for input
                    * requirements.txt
                - Libraries
                    * nothing

Arguments:    -Positional
                * [requirements]             // string
                * [outputDir]                // string

Observations:
            - 

TODO: - 

"""

import argparse
import json
from create_dir import create_dir
from build_ok_outputDir_string import build_ok_outputDir_string


def get_dic_of_software(afile):
    """
    Get a dict of software: version that belong from a requirements file.
    Parameters:
                    * afile: a string. Correspond to one requirements list.
    Observations:
                    * A line is like:
                        biopython==1.76
    Return: a dict.
    """
    software_for_release = dict()
    library_for_release = dict()
    standalone_for_release = dict()
    file = open(afile, 'r')
    for line in file.readlines():
        aSoftware = line.strip('\n')
        if aSoftware.startswith('biopython'):
            biopython = aSoftware.split('==')
            library_for_release.update({biopython[0]: biopython[1]})
        if aSoftware.startswith('gemmi'):
            gemmi = aSoftware.split('==')
            library_for_release.update({gemmi[0]: gemmi[1]})
        if aSoftware.startswith('matplotlib'):
            matplotlib = aSoftware.split('==')
            library_for_release.update({matplotlib[0]: matplotlib[1]})
        if aSoftware.startswith('numpy'):
            numpy = aSoftware.split('==')
            library_for_release.update({numpy[0]: numpy[1]})
        if aSoftware.startswith('pandas'):
            pandas = aSoftware.split('==')
            library_for_release.update({pandas[0]: pandas[1]})
        if aSoftware.startswith('scipy'):
            scipy = aSoftware.split('==')
            library_for_release.update({scipy[0]: scipy[1]})
        if aSoftware.startswith('seaborn'):
            seaborn = aSoftware.split('==')
            library_for_release.update({seaborn[0]: seaborn[1]})
    file.close()
    # Manually
    standalone_for_release.update({'BlastClust': 'v2.2.26'})
    standalone_for_release.update({'CD-HIT': 'v4.8.1'})
    standalone_for_release.update({'DSSR': 'v1.9.10'})
    standalone_for_release.update({'Gemmi': 'v0.3.6'})
    ## For python ---> sys.version_info[:3] ---> tuple of (3,6,9)
    standalone_for_release.update({'Python': 'v3.6.9'})
    standalone_for_release.update({'RNArtistCore': 'v0.2.7'})
    standalone_for_release.update({'TM-align': 'v20190425'})

    software_for_release.update({'standalone':standalone_for_release,  'python_libraries':library_for_release})
    return software_for_release


def get_dic_of_ext_databases():
    """
    Get a dictionary with external databases used in CoDNaS-RNA.
    Parameters: * nothing
    Return: dict
    """
    ext_databases_dict = dict()
    ext_databases_dict.update({'RCSB_PDB':  {'date': '2020-02-13',
                                             'url': 'https://www.rcsb.org/',
                                             'release': None,
                                             'message': 'All entries with at least one RNA chain are initially considered.'}
                              })
    ext_databases_dict.update({'RNAcentral': {'date': '2020-05-21',
                                              'url': 'https://rnacentral.org/',
                                              'release': '15',
                                              'message': 'Contains ~16M sequences, ~46M mapped entries, with links to 40+ databases.'}
                              })
    return ext_databases_dict


def get_release_json(requirements):
    """
    Create release_json dict to be used in CoDNaS-RNA website.
    Parameters:
                * requirements:        a string. Correspond to a path where requirements
                                                 file is located
                * outputDir:                 a string. Correspond to an output directory to store
                                                       release_20XX-XX.json file.
    Return: dict
    """
    release_json = dict()

    software_dict = get_dic_of_software(requirements)

    external_databases_dict = get_dic_of_ext_databases()

    release_json.update(
                        {'databases': external_databases_dict,
                         'software': software_dict}
    )
    return release_json


def create_release_file(requirements, outputDir):
    """
    Create and save release_' + '20XX-XX' + '.json' file to be used in CoDNaS-RNA website.
    Parameters:
                * requirements:        a string. Correspond to a path where requirements
                                                 file is located
                * outputDir:                 a string. Correspond to an output directory to store
                                                       release_20XX-XX.json file.
    Return: nothing
    """
    # Before start to do anything, check directories and if outputDir does not exists create it
    outputDir = build_ok_outputDir_string(outputDir)
    create_dir(outputDir)

    release_json = get_release_json(requirements)

    filename = 'release_' + '2021-09' + '.json'
    with open(outputDir + filename, 'w') as json_file:
        json.dump(release_json, json_file)

#    # Serializing json   
#    json_object = json.dumps(dictionary, indent = 4)  
#    print(json_object)

    print("All done !")

    return


# requirements = '/home/martingb/Projects/2020/CoDNaS-RNA/src/requirements/'
# outputDir = '/home/martingb/Projects/2020/CoDNaS-RNA/results/website/releases/2021-09/web/'


if __name__ == "__main__":

    p = argparse.ArgumentParser(
        description="Create release_20XX-XX.json file to be used in CoDNaS-RNA website."
    )
    # Positional arguments
    p.add_argument(
        "requirements",
        type=str,
        help="Path where requirements file is located."
    )
    p.add_argument(
        "outputDir",
        type=str,
        help="Path where to store the release_20XX-XX.json file."
    )

    args = p.parse_args()
    create_release_file(**vars(args))

# Written by Martín González Buitrón
# Email: martingonzalezbuitron@gmail.com