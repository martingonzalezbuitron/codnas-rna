"""\
Purpose: With this module you can encode or decode a string.

Preconditions:
                - nothing

Arguments:    -Positional
                * [string]          // string
                * [action]          // bool

Observations:
            - 

TODO: - 

"""

import argparse


def reveal_conf(string):
    """
    Reveal a string that is encoded.
    Parameters:
                * string:       a string. Correspond to a encoded
                                          conformer name.
    Return: a string
    """
    string = string.replace("%", "")
    string = string.replace("$", "")
    string = string.replace("#", "")
    return string


def hide_string(string):
    """
    Encode a string adding a sufix:
                           * '%' for upper characters.
                           * '$' for lower characters.
                           * '#' for digits.
    Parameters:
                * string:       a string. Correspond to an decoded
                                          conformer name.
    Return: a string
    """
    coding_secret = []
    for char in string:
        if char.isalpha():
            if char.isupper():
                coding_secret.append(char + "%")
            elif char.islower():
                coding_secret.append(char + "$")
        elif char.isdigit():
            coding_secret.append(char + "#")
        else:
            print("Error:\t" "unknown\t" + char)

    coding_secret = "".join(coding_secret)

    return coding_secret


def hide_conf(string):
    """
    Encode a conformer name.
    Parameters:
                * string:       a string. Correspond to an decoded
                                          conformer name.
    Return: a string
    """
    pdb, model, chain = string.split("_")
    model_hide = hide_string(model)
    chain_hide = hide_string(chain)
    string_hide = "_".join([pdb, model_hide, chain_hide])

    return string_hide


def encode(string, action):
    """
    Encode or decode a string.
    Parameters:
                * string:       a string. Correspond to an encoded
                                          or decoded conformer name.
    Return: a string
    """
    if action:
        conf_name = hide_conf(string)
    else:
        conf_name = reveal_conf(string)
    return conf_name


if __name__ == "__main__":

    p = argparse.ArgumentParser(
        description="Handle encryptation for a conformer name of CoDNaS-RNA."
    )
    # Positional arguments
    p.add_argument(
        "string",
        type=str,
        help='A string of characters without space and "_" separator.',
    )
    p.add_argument(
        "action",
        type=bool,
        default=True,
        help="An boolean value to encrypt or decode a string.",
    )

    args = p.parse_args()
    encode(**vars(args))

# Written by Martín González Buitrón
# Email: martingonzalezbuitron@gmail.com