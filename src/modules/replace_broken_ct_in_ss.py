#!/usr/bin/env python3
"""\
Purpose: With this module you can replace to a dot any broken basepairs and
         pseudoknots from a secondary structure (sstr) of a given extended
         Dot-Bracket Notation (dbn) Secondary Structure (SS) string.
         If missings are present (&) in sstr you can use "--remove_missings"
         option also (Default remove_missings=False).
         This will helps to plot nicelly RNA's 2D SS without readings problems.

Preconditions:
                - Files for input
                    * nothing
                - Libraries
                    * nothing

Arguments:    -Positional
                * [sstr]                           // string

Observations:
                *   ------------ For Testing ------------
                    bseq = "AUCCGUUCGGAUCA"
                    sstr_list = [ "((((((.))))))(("
                                , "((((.((.))))).)(("
                                , "(({((((.))}))))(("
                                , "(([]([(((.))))))(("
                                , "(([]([(((.A))))))((a"
                                , "]((([(((.a)))(((((((A"
                                , "..)))))).&...{..a."
                                , "(((((((..(((.....[.....))).((((.....A...)))).(((...)))...(((((..]....))))))))))))...."
                                , "..........(............)....(((..{....)))..........(...&......)...........&.B"
                                , "]]][[..." ]

                    for sstr in sstr_list:
                        print("GOT:" + "\n" + sstr)
                        sstr_clean = clean_ss(sstr)
                        print("DELIVERED:" + "\n" + sstr_clean)
                    -------------------------------------
TODO: - 

"""


import argparse


def get_dbn_char_dict(sstr):
    """
    Get a dictionary of characters as keys from sstr.
    Each value is the corresponded closed character.
    For missing character key and value are "&".
    Dots are not taken into account as a valid character.
    Parameters:
                * sstr:                 a string. Correspond to a sstr from a dbn.
    Return: a dict.
    """
    dbn_char_list = list({char for char in sstr})
    if "." in dbn_char_list:
        dbn_char_list.remove(".")
    dbn_char_dict = {}
    for char in dbn_char_list:
        if (char == "(") or (char == ")") and (char not in dbn_char_dict):
            dbn_char_dict.update({"(": ")"})
        elif (char == "[") or (char == "]") and (char not in dbn_char_dict):
            dbn_char_dict.update({"[": "]"})
        elif (char == "{") or (char == "}") and (char not in dbn_char_dict):
            dbn_char_dict.update({"{": "}"})
        elif (char == "<") or (char == ">") and (char not in dbn_char_dict):
            dbn_char_dict.update({"<": ">"})
        elif char.isalpha():
            if char.isupper():
                if char not in dbn_char_dict:
                    lower_char = char.lower()
                    dbn_char_dict.update({char: lower_char})
            else:
                upper_char = char.upper()
                if upper_char not in dbn_char_dict:
                    dbn_char_dict.update({char: upper_char})
        elif char == "&":
            dbn_char_dict.update({"&": "&"})
    return dbn_char_dict


def remove_missings_from_sstr(sstr):
    """
    Remove missings dbn characters from sstr.
    Parameters:
                * sstr:                 a string. Correspond to a sstr from a dbn.
    Return: a string.
    """
    sstr = [char for char in sstr if char != "&"]
    sstr = "".join(sstr)
    return sstr


def get_pos_char_matrix(sstr):
    """
    Get a dictionary of positions with "type" and empty "ct" (contact) as dict value.
    Example: {pos: {"type": char, "ct": "NA"}}
    Parameters:
                * sstr:                 a string. Correspond to a sstr from a dbn.
    Return: a dict.
    """
    return {pos: {"type": char, "ct": "NA"} for pos, char in enumerate(sstr)}



def save_missing(pos_char_matrix, pos):
    """
    Save missing position as a missing "ct" (contact) field of pos_char_matrix dictionary.
    Parameters:
                * pos_char_matrix:               a dict. Correspond to a dictionary of positions
                                                         with "type" and "ct" from a dbn.
                * sstr:                          a string. Correspond to a sstr from a dbn.
    Return: a dict.
    """
    pos_char_matrix[pos]["ct"] = "NA"
    return pos_char_matrix


def save_ct(pos_char_matrix, open_pos, close_pos):
    """
    Save contact position for open and close character of pos_char_matrix dictionary.
    Parameters:
                * pos_char_matrix:               a dict. Correspond to a dictionary of positions
                                                         with "type" and "ct" from a dbn.
                * open_pos:                      an int. Correspond to an index position from a
                                                         sstr.
                * close_pos:                     an int. Correspond to an index position from a
                                                         sstr.
    Return: a dict.
    """
    pos_char_matrix[close_pos]["ct"] = open_pos
    pos_char_matrix[open_pos]["ct"] = close_pos
    return pos_char_matrix


def annotate_cts(pos_char_matrix, dbn_char_dict, sstr):
    """
    Annotate "ct" (contact) field of pos_char_matrix dictionary.
    Parameters:
                * pos_char_matrix:               a dict. Correspond to a dictionary of positions
                                                         with "type" and empty "ct" from a dbn.
                * dbn_char_dict:                 a string. Correspond to a dictionary of characters
                                                           from the given sstr.
                * sstr:                          a string. Correspond to a sstr from a dbn.
    Return: a dict.
    """
    for dbn_char in list(dbn_char_dict):
        for pos, char in enumerate(sstr):
            if char != dbn_char_dict[dbn_char]:
                continue
            if char == "&":
                pos_char_matrix = save_missing(pos_char_matrix, pos)
                continue
            for rev_pos, rev_char in enumerate(reversed(sstr[:pos])):
                rev_pos = len(sstr[:pos]) - 1 - rev_pos
                if (rev_char == dbn_char) and (pos_char_matrix[rev_pos]["ct"] == "NA"):
                    pos_char_matrix = save_ct(pos_char_matrix, pos, rev_pos)
                    break
    return pos_char_matrix


def replace_brokes_with_dot(pos_char_matrix):
    """
    Replace broken contacts (ct="NA") of pos_char_matrix dictionary with dots.
    Parameters:
                * pos_char_matrix:               a dict. Correspond to a dictionary of positions
                                                         with "type" and "ct" from a dbn.
    Return: a dict.
    """
    for pos, data in pos_char_matrix.items():
        if data["type"] != "." and \
           data["type"] != "&" and \
           data["ct"] == "NA":
            pos_char_matrix[pos]["type"] = "."
    return pos_char_matrix


def get_sstr_clean(pos_char_matrix):
    """
    Get a clean sstr from pos_char_matrix dictionary.
    Parameters:
                * pos_char_matrix:               a dict. Correspond to a dictionary of positions
                                                         with "type" and "ct" from a dbn.
    Return: a string.
    """
    pos_char_matrix_clean = replace_brokes_with_dot(pos_char_matrix)
    sstr_clean = [data["type"] for data in pos_char_matrix_clean.values()]
    sstr_clean = "".join(sstr_clean)
    return sstr_clean


def clean_ss(sstr, remove_missings=False):
    """
    Clean a DSSR's secondary structure (sstr).
    Parameters:
                * sstr:                 a string. Correspond to a sstr from a dbn.
                * remove_missings:      a bool. Indicates if missings will be removed.
    Return: a string.
    """
    if remove_missings:
        sstr = remove_missings_from_sstr(sstr)
    dbn_char_dict = get_dbn_char_dict(sstr)
    pos_char_matrix = get_pos_char_matrix(sstr)
    pos_char_matrix = annotate_cts(pos_char_matrix, dbn_char_dict, sstr)
    sstr_clean = get_sstr_clean(pos_char_matrix)
    return sstr_clean


if __name__ == "__main__":


    p = argparse.ArgumentParser(
        description="Replace to a dot any broken basepairs and pseudoknots from dbn secondary strings."
    )
    # Positional arguments
    p.add_argument(
        "sstr",
        type=str,
        help="Correspond to a sstr from a dbn."
    )
    p.add_argument(
        "-r",
        "--remove_missings",
        action="store_true",
        default=False,
        type=bool,
        help="Remove missigns flags (&) if they are present in sstr."
    )

    args = p.parse_args()
    clean_ss(**vars(args))

# Written by Martín González Buitrón
# Email: martingonzalezbuitron@gmail.com