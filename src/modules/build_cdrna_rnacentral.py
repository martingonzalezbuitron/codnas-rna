"""\
Purpose: With this module you can create a full CoDNaS-RNA
         cdrna_rnacentral.csv.gz website table.

Preconditions:
                - Files for input
                    * nothing, just a directory where to find them
                - Libraries
                    * Pandas
                    * Numpy

Arguments:    -Positional
                * [rnacentralDir]                // string
                * [outputDir]                    // string

Observations:
            - Files in rnacentralDir:
                  * Clusters_RNAcentral_mapped/Cluster_#.tsv
                  * rnc_postgres_unique.tsv.gz

TODO: - Implement optional parameters for partially fill cdrna_rnacentral.csv.gz

"""


import argparse
import os
import pandas as pd
from build_ok_outputDir_string import build_ok_outputDir_string
from get_sorted_clstr_paths_list import get_sorted_clstr_paths_list
from save_website_table import save_website_table
from get_website_df import get_df_if_file_exist
from create_dir import create_dir


def create_dataframe_struct(rnacentralDir, outputDir):
    """
    Create a dataframe structure using Clusters_RNAcentral_mapped/Cluster_#.csv as templates.
    Parameters:
                * rnacentralDir:             a string. Correspond to a directory where all
                                                       RNAcentral tables are stored.
                * outputDir:                 a string. Correspond to an output directory to store
                                                       CoDNaS-RNA website tables.
    Return: a dataframe
    """
    # This is a FLAG to be faster to debug
    if os.path.exists(outputDir + "cdrna_rnacentral.csv.gz"):
        cdrna_rnacentral = get_df_if_file_exist(
            "cdrna_rnacentral", outputDir + "cdrna_rnacentral.csv.gz", ",", False
        )
        return cdrna_rnacentral

    # Directory to search RNAcentral data mapped
    rnacentralDir = build_ok_outputDir_string(rnacentralDir)

    cltr_dir = rnacentralDir + "Clusters_RNAcentral_mapped/"

    # Get a list of clusters csv files
    clusters_tsv_list = get_sorted_clstr_paths_list(cltr_dir, ".tsv")
    clusters_tsv_list_to_see = clusters_tsv_list.copy()

    columns_present = [
        "cluster_id",  # From RNAcentral filenames mapped
        "URS_ID",  # Table RNAcentral
        "DNA_md5",  # Table RNAcentral
        "Database_name",  # Table RNAcentral
        "Database_ID",  # Table RNAcentral
        "Taxon_ID",  # Table RNAcentral
        "RNA_type",  # Table RNAcentral
        "Gene_name"  # Table RNAcentral
        #                "rna_name",          # Table RNAcentral Postgres
        #                "specie"             # Table RNAcentral Postgres
    ]

    cdrna_rnacentral = pd.DataFrame(columns=columns_present)

    # Start to build cdrna_rnacentral
    while len(clusters_tsv_list_to_see) != 0:
        # Start to process each cluster
        afile = clusters_tsv_list_to_see.pop(0)
        cluster_id = os.path.basename(afile).split(".tsv")[0]
        cltr_rnacentral_table = get_df_if_file_exist(
            "cltr_rnacentral_table", afile, "\t", False
        )
        cltr_rnacentral_table["cluster_id"] = cluster_id
        # Add cltr table to cdrna_rnacentral dataframe
        cdrna_rnacentral = cdrna_rnacentral.append(
            cltr_rnacentral_table, ignore_index=True, sort=False
        )

    # Build flip dict
    flip_col_name_dict = {
        "URS_ID": "urs_id",
        "DNA_md5": "dna_md5",
        "Database_name": "database_name",
        "Database_ID": "database_id",
        "Taxon_ID": "taxon_id",
        "RNA_type": "rna_type",
        "Gene_name": "gene_name",
    }
    # Rename those columns
    cdrna_rnacentral = cdrna_rnacentral.rename(columns=flip_col_name_dict)

    # Add tow columns
    cltr_rnacentral_table["rna_name"] = "NA"
    cltr_rnacentral_table["specie"] = "NA"

    return cdrna_rnacentral


def merge_rnc_postgres(cdrna_rnacentral, rnacentralDir, a_separator, outputDir):
    """
    Merge rnc_postgres table to cdrna_rnacentral table.
    Parameters:
                * cdrna_rnacentral:          a dataframe. Correspond to a pandas dataframe for
                                                          CoDNaS-RNA rnacentral website table.
                * rnacentralDir:             a string. Correspond to a directory where all
                                                       RNAcentral tables are stored.
                * a_separator:               a string. Correspond to the rnc_postgres_unique.tsv.gz
                                                       table separator.
                * outputDir:                 a string. Correspond to an output directory to store
                                                       CoDNaS-RNA website tables.
    Return: a dataframe
    """
    # This is a FLAG to be faster to debug
    if os.path.exists(outputDir + "cdrna_rnacentral.csv.gz"):
        return cdrna_rnacentral

    # Directory to search RNAcentral data mapped
    rnacentralDir = build_ok_outputDir_string(rnacentralDir)

    # This is a FLAG to be faster to debug
    if os.path.exists(rnacentralDir + "rnc_postgres_unique.tsv"):
        postgres_table = get_df_if_file_exist(
            "postgres_table", rnacentralDir + "rnc_postgres_unique.tsv.gz", "\t", False
        )
    else:
        postgres_table = get_df_if_file_exist(
            "postgres_table",
            rnacentralDir + "rnc_postgres_unique_old.tsv.gz",
            a_separator,
            False,
        )

        # Postgres columns (here are in other order).
        col_postgres_dict = {
            "xref.upi": "urs_id",
            "acc.species": "specie",
            "acc.description": "rna_name",
            "xref.ac": "database_id",
        }

        # Rename those columns
        postgres_table = postgres_table.rename(columns=col_postgres_dict)

        # Remove duplicates on database_id column
        postgres_clean = postgres_table.loc[
            (postgres_table.duplicated("database_id") == False)
        ]

        # Save dataframe as a file.
        postgres_clean.to_csv(
            rnacentralDir + "rnc_postgres_unique.tsv",
            sep=a_separator,
            index=False,
            header=True,
        )

    # Remove duplicates on database_id column
    postgres_clean = postgres_table.loc[
        (postgres_table.duplicated("database_id") == False)
    ]

    # Critical step to merge dataframes using two columns ("database_id", "urs_id")
    cdrna_rnacentral_merge = pd.merge(
        cdrna_rnacentral, postgres_clean, on=["database_id", "urs_id"], how="left"
    )

    # Remove unused columns
    cdrna_rnacentral = cdrna_rnacentral_merge.drop(
        columns=[
            "acc.feature_name",
            "acc.classification",
            "rncdb.display_name",
            "name_specie",
        ]
    ).copy()

    # THis fix a mistake in cluster_id value names
    cdrna_rnacentral["cluster_id"] = (
        cdrna_rnacentral["cluster_id"].str.split(".tsv").str[0]
    )

    # Correct order
    # xref.upi    xref.ac    acc.feature_name    acc.description    acc.species    acc.classification    rncdb.display_name
    #    columns_postgres = [
    #                "xref.upi",           # urs_id
    #                "rncdb.display_name", # database_name
    #                "xref.ac",            # database_id
    #                "acc.species",        # related to taxon_id
    #                "acc.feature_name",   # rna class from database_id
    #                "acc.description",    # more informative rna name
    #                "acc.classification"  # taxonomy classsification
    #    ]

    return cdrna_rnacentral


def fill_cdrna_rnacentral(
    rnacentralDir, outputDir
):
    """
    Create and fill the website cdrna_rnacentral table.
    Parameters:
                * rnacentralDir:             a string. Correspond to a directory where all
                                                       RNAcentral tables are stored.
                * outputDir:                 a string. Correspond to an output directory to store
                                                       CoDNaS-RNA website tables.
    Return: nothing
    """

    # Before start to do anything, check if outputDir is correct and create it
    create_dir(outputDir)

    cdrna_rnacentral = create_dataframe_struct(rnacentralDir, outputDir)

    cdrna_rnacentral = merge_rnc_postgres(
        cdrna_rnacentral, rnacentralDir, "\t", outputDir
    )

    #    save_website_table(cdrna_rnacentral, outputDir, "cdrna_rnacentral", ",")

    #    exit

    print("All done !")

    return


# rnacentralDir = '/home/martingb/Projects/2020/CoDNaS-RNA/processed_data/2020-05-15/tables/RNAcentral/release-15/'
# outputDir = '/home/martingb/Projects/2020/CoDNaS-RNA/results/website/releases/2021-09/tables/'


if __name__ == "__main__":

    p = argparse.ArgumentParser(
        description="Create cdrna_rnacentral.csv.gz table to be used in CoDNaS-RNA website."
    )
    # Positional arguments
    p.add_argument(
        "rnacentralDir",
        type=str,
        help="Path where to store RNAcentral mapped data."
    )
    p.add_argument(
        "outputDir",
        type=str,
        help="Path where to store cdrna_rnacentral table."
    )

    args = p.parse_args()
    fill_cdrna_rnacentral(**vars(args))

# Written by Martín González Buitrón
# Email: martingonzalezbuitron@gmail.com