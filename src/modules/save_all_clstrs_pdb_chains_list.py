"""\
Purpose: With this function you can save a PDB:chain list from all
         CoDNaS-RNA's clusters in a file named "PDB_chains_from_clstrs.list".

Preconditions:
                - Files for input
                    * nothing
                - Libraries
                    * pandas

Arguments:    -Positional
                * [clusters_csv_list]       // list
                * [extention]               // string

Observations:
            - 

TODO: - 
"""


import argparse
from remove_if_exist import remove_if_exist
import pandas as pd


def save_all_clstrs_pdb_chains_list(clusters_csv_list, outputDir):
    """
    Save PDB:chain list from all clusters in outputDir.
    Parameters:
                    * clusters_csv_list:    a list of each cluster path.
                    * outputDir:            a string. Corresponding to
                                                      list directory.
    Return: nothing.
    """
    clusters_csv_list_to_see = clusters_csv_list.copy()
    # Define output file
    output_pdb_chain = outputDir + "PDB_chains_from_clstrs.list"
    # Remove this file first so everything is prepared for every database runing test
    remove_if_exist([output_pdb_chain])
    # Start to process each cluster
    while len(clusters_csv_list_to_see) != 0:
        file = clusters_csv_list_to_see.pop(0)
        cluster = pd.read_csv(file, dtype=str, header=0)
        pdbs_chains_list = cluster["Seq_code"].to_list()
        # Save pdb_code + chain results
        with open(output_pdb_chain, "a") as outfile:
            for pdb_chain in pdbs_chains_list:
                outfile.write("%s\n" % (pdb_chain))
    return


if __name__ == "__main__":

    p = argparse.ArgumentParser(
        description="Save PDB:chain list from all clusters in outputDir."
    )
    # Positional arguments
    p.add_argument(
        "clusters_csv_list",
        help="List of clusters *.csv paths."
    )
    p.add_argument(
        "outputDir",
        type=str,
        help="Output directory to save PDB_chains_from_clstrs.list file."
    )

    args = p.parse_args()
    save_all_clstrs_pdb_chains_list(**vars(args))

# Written by Martín González Buitrón
# Email: martingonzalezbuitron@gmail.com