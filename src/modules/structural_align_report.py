"""\
Purpose: With this module you can create a TSV strutural alignment
         report from all clusters in CoDNaS-RNA.
         The main purpose is to report an aproximate timing and size
         state to build then a run strategy.

Preconditions:
                - Inputs
                    * alignments_output_dir:      string, "Path where all alignments results are stored".
                    * cluster_combinations_dir:   string, "Path where all clusters combinations files are stored"
                                                           (Clusters_combinations/).
                    * outputDir:                  string, "Path to save alignment report tsv table".

                - Libraries
                    * pandas
                - Modules
                    * In-House: build_ok_outputDir_string

Arguments:    -Positional
                * [alignments_output_dir]     // string
                * [cluster_combinations_dir]  // string
                * [outputDir]                 // string

Observations:
            - TSV output file name is: "<folder-align-name>" + "_alignment_report.tsv"


TODO: - 
"""

import argparse
import glob
import os.path
import subprocess
import pandas as pd
from build_ok_outputDir_string import build_ok_outputDir_string


def get_rnaalign_outputs_list(alignments_output_dir, afolder):
    """
    Get a list of *.csv cluster paths sorted from Cluster 0 to latest
    one.
    Parameters:
                    * alignments_output_dir: a string. Correspond to output path
                                           directory where all *.out and *.pdb
                                           files from RNAalign are stored.
                    * afolder: a string. Correspond to one RNAalign output folder.
    Return: a list of *.csv cluster paths.
    """
    # Get a list of each cluster path sorted
    # (clusters converted to ints for proper sorting)
    clusters_folders_list = sorted(
        [
            int(os.path.basename(f).strip("Cluster_"))
            for f in glob.glob(alignments_output_dir + afolder + "Cluster_*")
        ], reverse=True
    )
    # Re-make names to get all correct paths
    clusters_folders_list = [
        alignments_output_dir + afolder + "Cluster_" + str(f) for f in clusters_folders_list
    ]
    return clusters_folders_list


def get_clusters(cltr_report_table, cluster_combinations_dir):
    """
    Fill cluster report table with number of cluster id names.
    Parameters:
                    * cltr_report_table: a dataframe. Correspond to all clusters
                                         with detail info.
                    * cluster_combinations_dir: a string. Corresponding to 
                                                a directory where all clusters
                                                combinations list are stored.
    Return: a dataframe (cltr_report_table).
    """
    # Get a list of each cluster sorted
    # (clusters converted to ints for proper sorting)
    cluster_combinations_list = sorted(
        [
            int(os.path.basename(f).strip("Cluster_").strip("_comb.list"))
            for f in glob.glob(cluster_combinations_dir + "Cluster_*")
        ], reverse=False
    )
    # Re-make names to get all correct names
    cluster_combinations_list = [
        "Cluster_" + str(f) for f in cluster_combinations_list
    ]
    for cltr_id in cluster_combinations_list:
        if not cltr_id in cltr_report_table['Cluster_ID'].to_list():
            data = [{'Cluster_ID': cltr_id}]
            cltr_report_table = cltr_report_table.append(data, ignore_index=True, sort=False)
        else:
            print('Error:'+'\t'+cltr_id+'\t'+'already present in RNAalign report table.')
    return cltr_report_table


def add_value_in_column_to_df(cltr_id, avalue, acolumn, cltr_report_table):
    """
    Add value in the cell of a column that correspond to a particular cluster id
    in a dataframe table.
    Parameters:
                    * cltr_id: a string. Correspond to one cluster id name.
                    * avalue: a undefining type. Correspond to a value to be
                              added in one cell from a column and cluster id row.
                    * acolumn: a string. Correspond to one column name.
                    * cltr_report_table: a dataframe. Correspond to all clusters
                                         with detail info.

    Return: nothing.
    """
    #Adding new database code value
    cltr_report_table.loc[cltr_report_table['Cluster_ID'] == cltr_id, acolumn] = avalue
    return

def get_set_of_models(afile):
    """
    Get a set of models that belong from a cluster combination file.
    Parameters:
                    * afile: a string. Correspond to one
                             cluster combination list.
    Return: a set of models.
    """
    file = open(afile, 'r')
    models = set()
    for line in file.readlines():
        line = line.strip('\n').split(' ')
        for model in line:
            models.add(model)
    file.close()
    return models
#   return {model for line in afile.readline() for model in line.split(' ')}


def get_set_of_chains(set_models):
    """
    Get a set of chains that belong from a set of models.
    Parameters:
                    * set_models: a set. Correspond to all models
                                  present in one cluster.
    Return: a set of chains.
    """
#    chains = set()
#    for model in set_models:
#        pdb_code = model.split('_')[0]
#        chain = model.split('_')[2].split('.')[0]
#        pdb_code_chain = pdb_code + ':' + chain
#        chains.add(pdb_code_chain)
#    return chains
    return {model.split('_')[0]+':'+model.split('_')[2].split('.')[0] for model in set_models}


def get_number_of_pairs_models_and_chains(cltr_report_table, cluster_combinations_dir):
    """
    Fill cluster report table with number of pairs models and chains.
    Parameters:
                    * cltr_report_table: a dataframe. Correspond to all clusters
                                         with detail info.
                    * cluster_combinations_dir: a string. Corresponding to 
                                                a directory where all clusters
                                                combinations list are stored.
    Return: a dataframe (cltr_report_table).
    """
    # Get a list of each cluster sorted
    # (clusters converted to ints for proper sorting)
    cluster_combinations_list = sorted(
        [
            int(os.path.basename(f).strip("Cluster_").strip("_comb.list"))
            for f in glob.glob(cluster_combinations_dir + "Cluster_*")
        ], reverse=False
    )
    # Re-make names to get all correct names
    cluster_combinations_list = [
        cluster_combinations_dir + "Cluster_" + str(f) + '_comb.list' for f in cluster_combinations_list
    ]
    for cltr_comb_file in cluster_combinations_list:
        file = open(cltr_comb_file, 'r')
        total_comb = sum(1 for line in file)
        set_models = get_set_of_models(cltr_comb_file)
        total_models = len(set_models)
        set_chains = get_set_of_chains(set_models)
        total_chains = len(set_chains)
        cltr_id = os.path.basename(cltr_comb_file).strip("_comb.list")
        if cltr_id in cltr_report_table['Cluster_ID'].to_list():
            add_value_in_column_to_df(cltr_id, total_comb, 'Total_combinations', cltr_report_table)
            add_value_in_column_to_df(cltr_id, total_models, 'Total_models', cltr_report_table)
            add_value_in_column_to_df(cltr_id, total_chains, 'Total_conformers', cltr_report_table)
        else:
            print('Error:'+'\t'+cltr_id+'\t'+'not present in RNAalign report table.')
        file.close()
    return cltr_report_table


def get_time(alignments_output_dir, cltr_report_table):
    """
    Fill cluster report table with alignment time.
    Parameters:
                    * alignments_output_dir: a string. Corresponding to
                                             a directory where all alignmets
                                             are stored.
                    * cltr_report_table: a dataframe. Correspond to all clusters
                                         with detail info.
    Return: a dataframe (cltr_report_table).
    """
    clusters_folders_list = get_rnaalign_outputs_list(alignments_output_dir, "stdout/")
    for cltr_output_file in clusters_folders_list:
        cltr_id = os.path.basename(cltr_output_file)
        cmd = 'grep -R \"Total CPU time\" ' + cltr_output_file
        #Get a list of grep matches
        grep_lines = subprocess.getoutput(cmd).split('\n')
        #Report failed URS mapped cases
        if grep_lines[0] == '':
            print('Error: not time line found it in'+'\t'+cltr_output_file)
        elif len(grep_lines) > 1:
            print('Error: more than 1 time line found it in'+'\t'+cltr_output_file)
        else:
            grep_line = grep_lines[0].split(':')[1]
            grep_line = " ".join(grep_line.split())
            #Is a float
            time_pair = float(grep_line.split(' ')[4])
            #Check if time_pair = 0.00 correspond to real alignments.
            if time_pair == 0.00:
                #Search if you already have stored the size per sup file.
                if not pd.isnull(cltr_report_table['Size_per_pair_bytes'].loc[cltr_report_table['Cluster_ID'] == cltr_id].to_list()[0]):
                    time_pair = 0.009
                else:
#                    print('Error:'+'\t'+'empty size value in'+'\t'+cltr_id)
                    time_pair = None

            add_value_in_column_to_df(cltr_id, time_pair, 'Time_per_pair_seconds', cltr_report_table)
    return cltr_report_table


def get_size(alignments_output_dir, cltr_report_table):
    """
    Fill cluster report table with alignment size.
    Parameters:
                    * alignments_output_dir: a string. Corresponding to
                                             a directory where all alignmets
                                             are stored.
                    * cltr_report_table: a dataframe. Correspond to all clusters
                                         with detail info.
    Return: a dataframe (cltr_report_table).
    """
    clusters_folders_list = get_rnaalign_outputs_list(alignments_output_dir, "Sup_files/")
    for cltr_output_file in clusters_folders_list:
        cltr_id = os.path.basename(cltr_output_file)
        sup_file_list = glob.glob(cltr_output_file + '/*.pdb')
#        cmd = 'find '+cltr_output_file+' -type f -name \"*.pdb\" -printf \"%p %k KB\n\"'
        #Check what was found it
        if len(sup_file_list) == 0:
            print('Error: not superposition file found it in'+'\t'+cltr_id)
            #Get size in bytes (is a int)
            sup_size_bytes = None
            add_value_in_column_to_df(cltr_id, sup_size_bytes, 'Size_per_pair_bytes', cltr_report_table)
        elif len(sup_file_list) > 1:
            print('Error: more than 1 superposition file found it in'+'\t'+cltr_id)
        else:
            #Get size in bytes (is a int)
            sup_size_bytes = os.path.getsize(sup_file_list[0])
            add_value_in_column_to_df(cltr_id, sup_size_bytes, 'Size_per_pair_bytes', cltr_report_table)
    return cltr_report_table


def get_total_time_and_size(cltr_report_table):
    """
    Fill cluster report table with total time and size.
    Parameters:
                    * cltr_report_table: a dataframe. Correspond to all clusters
                                         with detail info.
    Return: a dataframe (cltr_report_table).
    """
    for cltr_id in cltr_report_table['Cluster_ID'].to_list():

        #Search if you already have stored the total combinations value
        if not pd.isnull(cltr_report_table['Total_combinations'].loc[cltr_report_table['Cluster_ID'] == cltr_id].to_list()[0]):
            total_comb = cltr_report_table['Total_combinations'].loc[cltr_report_table['Cluster_ID'] == cltr_id].to_list()[0]
        else:
            print('Error:'+'\t'+'empty total combinations value in'+'\t'+cltr_id)

        #Search if you already have stored the time per pair value
        if not pd.isnull(cltr_report_table['Time_per_pair_seconds'].loc[cltr_report_table['Cluster_ID'] == cltr_id].to_list()[0]):
            time_pair = cltr_report_table['Time_per_pair_seconds'].loc[cltr_report_table['Cluster_ID'] == cltr_id].to_list()[0]
        else:
#            print('Error:'+'\t'+'empty time per pair value in'+'\t'+cltr_id)
            time_pair = None

        #Search if you already have stored the size per sup file.
        if not pd.isnull(cltr_report_table['Size_per_pair_bytes'].loc[cltr_report_table['Cluster_ID'] == cltr_id].to_list()[0]):
            sup_size_bytes = cltr_report_table['Size_per_pair_bytes'].loc[cltr_report_table['Cluster_ID'] == cltr_id].to_list()[0]
        else:
#            print('Error:'+'\t'+'empty size value in'+'\t'+cltr_id)
            sup_size_bytes = None

        if time_pair == None:
            total_time_min = None
        else:
            total_time_min = round(float((time_pair*total_comb)/60.000), 4)
        if sup_size_bytes == None:
            total_size_mb = None
        else:
            total_size_mb = round(float((sup_size_bytes*total_comb)/(1024.000*1024.000)), 3)

        add_value_in_column_to_df(cltr_id, total_time_min, 'Total_Min', cltr_report_table)
        add_value_in_column_to_df(cltr_id, total_size_mb, 'Total_MB', cltr_report_table)
    return cltr_report_table


def get_rnaalign_tsv(alignments_output_dir, cluster_combinations_dir, outputDir):
    """
    Save an alignment TSV report.
    Parameters:
                    * alignments_output_dir: a string. Corresponding to
                                             a directory where all alignmets
                                             are stored.
                    * cluster_combinations_dir: a string. Corresponding to 
                                                a directory where all clusters
                                                combinations list are stored.
                    * outputDir: a string. Corresponding to a path to save results.
    Return: nothing.
    """
    #Get name of folder alignment results
    alignments_output_dir = build_ok_outputDir_string(alignments_output_dir)
    folder_align_name = os.path.basename(os.path.dirname(alignments_output_dir))

    #Directory to save mapping results
    outputDir = build_ok_outputDir_string(outputDir)

    #Build empty dataframe to fill it.
    columns = ['Cluster_ID','Total_conformers','Total_models','Total_combinations','Time_per_pair_seconds','Size_per_pair_bytes','Total_Min','Total_MB']
    cltr_report_table = pd.DataFrame(columns = columns)

    #Get clusters
    cltr_report_table = get_clusters(cltr_report_table, cluster_combinations_dir)

    #Get total combinations, models and chains
    cltr_report_table = get_number_of_pairs_models_and_chains(cltr_report_table, cluster_combinations_dir)

    #Get size per pair
    cltr_report_table = get_size(alignments_output_dir, cltr_report_table)

    #Get time per pair
    cltr_report_table = get_time(alignments_output_dir, cltr_report_table)

    #Get total time and size per cluster
    cltr_report_table = get_total_time_and_size(cltr_report_table)

    #Save report
    cltr_report_table.to_csv(outputDir + folder_align_name + "_alignment_report.tsv", sep='\t', index=False, header=True)
    return

if __name__ == "__main__":

    p = argparse.ArgumentParser(description="Create an alignment tsv table reporting detail info per cluster to get:\n -Number of models\n -Number of pairs\n -Calculation time\n -Total required space")
    # Positional arguments
    p.add_argument(
        "alignments_output_dir",
        type=str,
        help="Path to alignments results."
    )
    p.add_argument(
        "cluster_combinations_dir",
        type=str,
        help="Path to Cluster combinations lists."
    )
    p.add_argument(
        "outputDir",
        type=str,
        help="Path to save alignment report tsv table."
    )

    args = p.parse_args()
    get_rnaalign_tsv(**vars(args))

# Written by Martín González Buitrón
# Email: martingonzalezbuitron@gmail.com