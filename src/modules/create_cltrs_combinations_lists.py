"""\
Purpose: With this module you can create PDB-MODEL-CHAIN combination list files
         using clustering and mmCIF files.
         It creates a folder as: "Clusters_combinations/" with all
         "Cluster_#_comb.list" files inside.

Preconditions:
                - Files for input
                    * cif files
                - Libraries
                    * Pandas

Arguments:    -Positional
                * [clusters_csv_list]              // list
                * [cif_dir]                        // string
                * [outputDir]                      // string

Observations: -

TODO: - 
"""

import argparse
import os
import pandas as pd
from itertools import combinations
from build_ok_outputDir_string import build_ok_outputDir_string
from remove_if_exist import remove_if_exist
from get_pdb_models_of_chain_list import get_pdb_models_of_chain_list

# I'm not sure why I need it because for combinations is better per cluster
# and only in this format:
# PDB-MODEL#-CHAIN1    PDB-MODEL#-CHAIN1
# PDB-MODEL#-CHAIN1    PDB-MODEL#-CHAIN1
# PDB-MODEL#-CHAIN1    PDB-MODEL#-CHAIN1
def save_cltr_pdb_chain_list(clusters_csv_list, outputDir):           # Unused function
    """
    Save CLUSTER ID + PDB + CHAIN list from all clusters in outputDir.
    Parameters:
                    * clusters_csv_list:    a list of each cluster path.
                    * outputDir:            a string. Directory to save combination list files.
    Returns: nothing.
    """
    #clusters_csv_list_to_see = [file for file in clusters_csv_list]
    clusters_csv_list_to_see = clusters_csv_list.copy()
    # Define output file
    output_cltr_pdb_chain = outputDir + "Clusters_pdbs_chains_from_clstrs.list"
    # Remove this file first so everything is prepared for every database runing test
    print(output_cltr_pdb_chain)
    remove_if_exist([output_cltr_pdb_chain])
    # Start to process each cluster
    while len(clusters_csv_list_to_see) != 0:
        file = clusters_csv_list_to_see.pop(0)
        cluster = pd.read_csv(file, dtype=str, header=0)
        cluster_id = os.path.basename(file).strip(".csv")
        pdbs_chains_list = cluster["PDB_Chain"].to_list()
        # Save cluster + pdb_code + chain results
        with open(output_cltr_pdb_chain, "a") as outfile:
            for pdb_chain in pdbs_chains_list:
                outfile.write(
                    "%s %s %s\n"
                    % (cluster_id, pdb_chain.split(":")[0], pdb_chain.split(":")[1])
                )
    return


def get_all_cluster_model_chains_list(alist, cif_dir):
    """
    Get a list of all PDB MODEL CHAINs combinations from one cluster
    using *.cif files located in cif_dir/ to search corresponding models.
    Parameters:
                    * alist:                a list of PDB:Chains from one cluster.
                    * cif_dir:              a string. Correspond to path directory
                                                      where all cif files are stored.
    Returns: a list of PDB MODEL CHAIN names.
    """
    cltr_model_filenames = []
    for pdb_chain in alist:
        for model_name in get_pdb_models_of_chain_list(
            pdb_chain, cif_dir
        ):
            cltr_model_filenames.append(model_name)
    return cltr_model_filenames


def save_cltr_combinations(chain_combinations, cluster_id, outputDirComb):
    """
    Save cluster combination list in outputDir.
    Parameters:
                    * chain_combinations:   a list of PDB MODEL CHAIN combinations.
                    * cluster_id:           a string. Correspond to cluster id.
                    * outputDirComb:        a string. Directory to save combination list files.
    Returns: nothing.
    """
    # Make dir or do nothing if exists
    os.makedirs(outputDirComb, exist_ok=True)
    # Save each cluster pdb_code + model_chain combination results
    with open(outputDirComb + cluster_id + "_comb.list", "a") as outfile:
        for comb in chain_combinations:
            outfile.write("%s %s\n" % (comb[0], comb[1]))
    return


def create_cltrs_combinations_lists(clusters_csv_list, cif_dir, outputDir):
    """
    Create every cluster combination list using *.cif files located in
    cif_dir/ to search corresponding models and finally save those list
    files in outputDir.
    Parameters:
                    * clusters_csv_list:    a list of each cluster path.
                    * cif_dir:              a string. Correspond to path directory
                                                      where all cif files are stored.
                    * outputDir:            a string. Directory to save Clusters_combinations/.
    Returns: nothing.
    """
    clusters_csv_list_to_see = clusters_csv_list.copy()
    # Define output file
    outputDirComb = outputDir + "Clusters_combinations/"
    # Check and build outputDir correctly
    outputDirComb = build_ok_outputDir_string(
        outputDirComb
    )
    # Remove this directory first so everything is prepared for every database running test
    remove_if_exist([outputDirComb])
    # Start to process each cluster
    while len(clusters_csv_list_to_see) != 0:
        file = clusters_csv_list_to_see.pop(0)
        cluster = pd.read_csv(file, dtype=str, header=0)
        cluster_id = os.path.basename(file).strip(".csv")
        pdbs_chains_list = cluster["PDB_Chain"].to_list()
        clstr_model_chains_list = get_all_cluster_model_chains_list(
            pdbs_chains_list, cif_dir
        )
        chain_combinations = {comb for comb in combinations(clstr_model_chains_list, 2)}
        #    chain_combinations = [clstr_id"_"comb[0]+"_"+comb[1] for comb in chain_combinations]
        #    chain_combinations = [comb[0]+"_"+comb[1] for comb in chain_combinations]
        save_cltr_combinations(chain_combinations, cluster_id, outputDirComb)
    return


if __name__ == "__main__":

    p = argparse.ArgumentParser(
        description="Create every cluster combination list using *.cif files."
    )
    # Positional arguments
    p.add_argument(
        "clusters_csv_list",
        help="List of clusters *.csv paths."
    )
    p.add_argument(
        "cif_dir",
        type=str,
        help="Output directory to search *.cif file."
    )
    p.add_argument(
        "outputDir",
        type=str,
        help="Output directory to save all model files extrated."
    )

    args = p.parse_args()
    create_cltrs_combinations_lists(**vars(args))

# Written by Martín González Buitrón
# Email: martingonzalezbuitron@gmail.com