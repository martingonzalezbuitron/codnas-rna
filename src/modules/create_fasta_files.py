"""\
Purpose: With this module you can create all fasta files from
         cdrna_conformers.csv.gz of CoDNaS-RNA.

Preconditions:
                - Files for input
                    * cdrna_clusters.csv.gz
                    * cdrna_conformers.csv.gz
                - Libraries
                    * Pandas
                    * Numpy

Arguments:    -Positional
                * [website_tables_dir]             // string
                * [outputDir]                      // string

Observations:
            - 

TODO: - 

"""

import argparse
import os
import pandas as pd
from get_website_df import get_df_if_file_exist
from create_dir import create_dir
from build_ok_outputDir_string import build_ok_outputDir_string


def get_clusters_list(website_tables_dir):
    """
    Get a list of clusters located in CoDNaS-RNA.
    Parameters:
                * website_tables_dir:        a string. Correspond to a path where are stored all
                                                       CoDNaS-RNA website tables.
    Returns: a list
    """
    # Get a dataframe from table
    if os.path.exists(website_tables_dir + "cdrna_clusters.csv.gz"):
        cdrna_clusters = get_df_if_file_exist(
            "cdrna_clusters", website_tables_dir + "cdrna_clusters.csv.gz", ",", True
        )
    else:
        raise FileNotFoundError('[Error] File not exists: {}.'.format(website_tables_dir + 'cdrna_clusters.csv.gz'))

    #Create a list of cluster names sorted (is better handle cluster numbers)
    cltrs_list = sorted([int(cltr_id.strip("Cluster_")) for cltr_id in cdrna_clusters['cluster_id'].to_list() if not pd.isna(cltr_id)], reverse=True)
    # Re-make names to get all correct names
    cltrs_list = ["Cluster_" + str(cltr_id) for cltr_id in cltrs_list]
    
    return cltrs_list


def create_sequence_data(website_tables_dir, cltrs_list, outputDir):
    """
    Read cdrna_conformers.csv.gz and save fasta files. One per conformer located in CoDNaS-RNA.
    Parameters:
                * website_tables_dir:        a string. Correspond to a path where are stored all
                                                       CoDNaS-RNA website tables.
                * cltrs_list:                a list. Is a list of clusters_<ID>.
                * outputDir:                 a string. Correspond to an output directory to store
                                                       all fasta files.
    Return: nothing
    """
#- <ID-HASH> (sequence_data):
#    <ID-CONF_MODEL_CHAIN.FMT> (fasta)

    # Get a dataframe from table
    if os.path.exists(website_tables_dir + "cdrna_conformers.csv.gz"):
        cdrna_conformers = get_df_if_file_exist(
        "cdrna_conformers", website_tables_dir + "cdrna_conformers.csv.gz", ",", True
        )

    for cltr_id in cltrs_list:
        conf_subset = (
            cdrna_conformers[cdrna_conformers["cluster_id"] == cltr_id]
        )
        cltr_df = pd.DataFrame()
        cltr_df['pdb_model_chain'] = conf_subset.pdb_model_chain
        cltr_df['fasta_lines'] = ">" + conf_subset.pdb_model_chain.astype(str) + "\n" + conf_subset.seqres + "\n"

        for a_conformer in cltr_df['pdb_model_chain']:
            filename = outputDir + a_conformer + ".fasta"
            afasta = cltr_df.loc[cltr_df['pdb_model_chain'] == a_conformer, 'fasta_lines'].to_list()[0]
            with open(filename, "w") as output:
                output.write(afasta)
    return


def create_files(website_tables_dir, outputDir):
    """
    Create fasta files. One per conformer located in CoDNaS-RNA.
    Parameters:
                * website_tables_dir:        a string. Correspond to a path where are stored all
                                                       CoDNaS-RNA website tables.
                * outputDir:                 a string. Correspond to an output directory to store
                                                       all fasta files.
    Return: nothing
    """
    # Before start to do anything, check direcotories and if outputDir does not exists create it
    outputDir = build_ok_outputDir_string(outputDir)
    website_tables_dir = build_ok_outputDir_string(website_tables_dir)

    create_dir(outputDir)

    cltrs_list = get_clusters_list(website_tables_dir)

    create_sequence_data(website_tables_dir, cltrs_list, outputDir)

    print("All done !")
    return

# website_tables_dir = '/home/martingb/Projects/2020/CoDNaS-RNA/results/website/releases/2021-09/tables/'
# outputDir = '/home/martingb/Projects/2020/CoDNaS-RNA/processed_data/2020-05-15/fastas/'


if __name__ == "__main__":

    p = argparse.ArgumentParser(
        description="Create fasta files from each CoDNaS-RNA conformer to be used in CoDNaS-RNA website."
    )
    # Positional arguments
    p.add_argument(
        "website_tables_dir",
        type=str,
        help="Path where all website tables are located."
    )
    p.add_argument(
        "outputDir",
        type=str,
        help="Path where to store all fasta files."
    )

    args = p.parse_args()
    create_files(**vars(args))

# Written by Martín González Buitrón
# Email: martingonzalezbuitron@gmail.com