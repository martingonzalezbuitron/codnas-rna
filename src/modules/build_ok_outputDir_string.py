"""\
Purpose: With this function you can fix a string to be used for a directory.

Preconditions:
                - nothing

Arguments:    -Positional
                * [outputDir]          // string

Observations:
            - 
TODO: - 
"""

import argparse
import os.path


def build_ok_outputDir_string(outputDir):
    """
    Create a directory string.
    Parameters:
                   * outputDir: a string. Correspond to output path
                                          directory.
    Return: a string that correspond to *outputDir*.
    """
    # Get basename
    basename = os.path.basename(outputDir)
    # Get dirname
    dirname = os.path.dirname(outputDir)
    # Check basename to build outputDir Ok
    if basename != "":
        outputDir = dirname + "/" + basename + "/"
    else:
        outputDir = dirname + "/"
    return outputDir


if __name__ == "__main__":

    p = argparse.ArgumentParser(description="Create correctly a directory string."
    )
    # Positional arguments
    p.add_argument(
        "outputDir",
        type=str,
        help="String output directory to check if is it Ok."
    )

    args = p.parse_args()
    build_ok_outputDir_string(**vars(args))

# Written by Martín González Buitrón
# Email: martingonzalezbuitron@gmail.com