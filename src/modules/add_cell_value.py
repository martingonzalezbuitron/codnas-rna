"""\
Purpose: With this function you can add a value in a specific column of a
         dataframe subset.

Preconditions:
                - Files for input
                    * Pandas Dataframe
                - Libraries
                    * Pandas

Arguments:    -Positional
                * [adataframe]      // pandas dataframe
                * [df_value]        // <any type>
                * [df_column]       // string
                * [avalue]          // <any type>
                * [acolumn]         // string

Observations:
            - 

TODO: - 

"""

import argparse
import pandas as pd


def add_value_in_column_to_subset(adataframe, df_value, df_column, avalue, acolumn):
    """
    Save a particular value in a selected cell from specific dataframe subset.
    Parameters:
                * adataframe: a dataframe. Correspond to a dataframe where value
                                           is going to be saved it.
                * df_value: a value. Correspond to a value used as filter subset.
                * df_column: a string. Correspond to a dataframe column used as
                                       filter subset.
                * avalue: a value. Correspond to a value to be save it.
                * acolumn: a column. Correspond to a dataframe column where to
                                     save the "avalue".
    Return: nothing
    """
    adataframe.loc[adataframe[df_column] == df_value, acolumn] = avalue
    return


if __name__ == "__main__":

    p = argparse.ArgumentParser(description="Add value in a dataframe cell.")
    # Positional arguments
    p.add_argument(
        "adataframe",
        help="A pandas dataframe."
    )
    p.add_argument(
        "df_value",
        help="A dataframe column value to be used as filter subset."
    )
    p.add_argument(
        "df_column",
        type=str,
        help="A dataframe column to be used as filter subset."
    )
    p.add_argument(
        "avalue",
        help="A value to be saved."
    )
    p.add_argument(
        "acolumn",
        type=str,
        help="A dataframe column where to save the new value."
    )

    args = p.parse_args()
    add_value_in_column_to_subset(**vars(args))

# Written by Martín González Buitrón
# Email: martingonzalezbuitron@gmail.com