"""\
Purpose: With this module you can generate superclusters from two
         sequence clustering conditions.
         The final output are: two csv files, one corresponds to
         superclusters_info.csv and the other to those clusters
         that never cluster in both conditions.

Preconditions:
                - File for input
                    * CSV file     // file generated from AWK MGB script.
                - Libraries
                    * pandas

Arguments:    -Positional
                * [csv_file]       // file
                * [outputDir]      // string
              -Optional
                * "-r", "--report"    // boolean, default=False

Observations:
            - 
TODO: Improve to use one main dictionary, clusters = {"strict":strict, "relax":relax}

"""

import argparse
import pandas as pd
import os
import csv


def get_corresponding_clusters(set_seq, dict_seq_cluster):
    """
    Get a set of clusters.
    Parameters:
            * set_seq: a set of seqs.
            * dict_seq_cluster: a dict seq-cluster
                                from one condition.
    Return: a set of cluster names.
    """
    return {dict_seq_cluster[seq] for seq in set_seq}


def get_corresponding_seq(set_clusters, dict_condition):
    """
    Get a set of seqs.
    Parameters:
            * set_clusters: a set of cluster names.
            * dict_condition: a dict from one condition.
    Return: a set of sequence codes.
    """
    return {seq for cltr in set_clusters for seq in dict_condition[cltr][0].keys()}


def get_supercluster_name_format(set_clusters, char):
    """
    Get a supercluster name.
    Parameters:
               * set_clusters: a set of cluster names.
               * char: a string. ( Must be: O, M or N)
    Return: a string describing supercluster name with format.
    """
    # Convert to int for sort
    set_clusters = [int(cltr) for cltr in set_clusters]
    # Get only one cluster name to be used
    supercluster_name = sorted(set_clusters)[0]
    # Create format name for supercluster_name
    supercluster_name = f"{char}{supercluster_name:04d}"
    return supercluster_name


def build_supercluster_name(set_clusters, dict_condition):
    """
    Get a supercluster name.
    Parameters:
            * set_clusters: a set of cluster names.
            * dict_condition: a dict from one condition.
    Return: a string supercluster name formated.
    """
    if len(set_clusters) == 1:
        # Get the only cluster name
        supercluster_name = get_supercluster_name_format(set_clusters, "O")
    elif len(set_clusters) > 1:
        # Check if these clusters build a NEW or MODIFIED supuercluster.
        # First, check if they create a NEW cluster. For that reason,
        # we check the sum of sizes (always are size 1). If they create
        # a new supercluster the number of clusters need to be equal than
        # the sum of each cluster size.
        sum_cluster_sizes = sum(dict_condition[cltr][1] for cltr in set_clusters)
        if len(set_clusters) == sum_cluster_sizes:
            # Get one and lower cluster name
            supercluster_name = get_supercluster_name_format(set_clusters, "N")
        else:
            # Get one and lower cluster name
            supercluster_name = get_supercluster_name_format(set_clusters, "M")
    return supercluster_name


def buildDictFromCondition_In_(condition, adataframe):
    """
    Create a dictionary using one clustering condition
    from a adataframe that was created with pandas from
    MGB awk script.
    Parameters:
                * condition: a number denoting file '1' or '2'.
                * adataframe: a dataframe. Correspond to output table.csv
                              from MGB awk script.
    Return: a dict with info from one condition.
    """
    dict_name = dict()
    cluster_number = "cluster_number_in_file_" + str(condition)
    cluster_size = "cluster_size_in_file_" + str(condition)
    query_code = "query_code_in_file_" + str(condition)
    query_length = "query_length_in_file_" + str(condition)
    # Get list of clusters per condition
    list_of_clusters_file = adataframe[cluster_number].unique().tolist()
    for cluster_key in list_of_clusters_file:
        cluster = adataframe[adataframe[cluster_number].isin([cluster_key])][
            [query_code, query_length, cluster_size]
        ]
        # dict(zip(list(df.col1), list(df.col2)))
        dict_query_code_length_cluster = dict(
            zip(cluster[query_code].tolist(), cluster[query_length].tolist())
        )
        dict_query_code_length_cluster.update(dict_query_code_length_cluster)
        value_cluster_size = cluster[cluster_size].unique()[0]
        cluster_value = [dict_query_code_length_cluster, value_cluster_size]
        dict_name.update({cluster_key: cluster_value})
    return dict_name


def build_ok_outputDir_string(outputDir):
    """
    Create a directory string.
    Parameters:
                   * outputDir: a string. Correspond to output path
                                          directory.
    Return: a string that correspond to *outputDir*.
    """
    # Get basename
    basename = os.path.basename(outputDir)
    # Get dirname
    dirname = os.path.dirname(outputDir)
    # Check basename to build outputDir Ok
    if basename != "":
        outputDir = dirname + "/" + basename + "/"
    else:
        outputDir = dirname + "/"
    return outputDir


def save_superclustering_results(dict_sc, dict_nc, outputDir):
    """
    Create and save two csv files. One of superclusters from
    table_seq_*_info.csv and another csv with the seqs that
    never cluster.
    Parameters:
                * dict_sc: a dict. Correspond to superclusters.
                * dict_nc: a dict. Correspond to never_cluster.
                * outputDir: a string. Correspond to putput directory.
    Return: two csv files. (builded from dict_sc and dict_nc)
    """
    # Check and build outputDir correctly
    outputDir = build_ok_outputDir_string(outputDir)
    # Make dir or do nothing if exists
    os.makedirs(outputDir, exist_ok=True)
    # Build output filenames
    outfile_sc = outputDir + "superclusters_info.csv"
    outfile_nc = outputDir + "never_cluster_info.csv"
    outfilenames = [outfile_sc, outfile_nc]
    for dictionary in [dict_sc, dict_nc]:
        file = outfilenames.pop(0)
        # Save superclustering results
        with open(file, "w") as outfile:
            fieldnames = ["query_code", "query_length", "cluster_name", "cluster_size"]
            writer = csv.DictWriter(outfile, fieldnames=fieldnames)
            writer.writeheader()
            for sc in dictionary.keys():
                for query_code, query_length in dictionary[sc][0].items():
                    sc_size = dictionary[sc][1]
                    outfile.write(
                        "%s,%s,%s,%s\n" % (query_code, query_length, sc, sc_size)
                    )
    return


def get_list_superclusters_type(dict_sc, char):
    """
    Get a list of one supercluster type.
    Parameters:
                * dict_sc: a dict. Correspond to superclusters.
                * char: a string. Correspond to one supercluster letter.
    Return: a list of one supercluster type.
    """
    char = char.upper()
    list_sc = [cltr for cltr in dict_sc.keys() if char in cltr]
    return list_sc


def build_superclusters(csv_file, outputDir, report):
    """
    Get two CSV files. One of superclusters from
    table_seq_*_info.csv and another csv with the
    seqs that never cluster.
    Parameters:
                * csv_file: a file. Correspond to output table.csv
                            from MGB awk script.
                * outputDir: a string. Correspond to an output directory.
    Return: two csv files. (builded from superclusters and dict_never_cluster)
    """
    csv_file = pd.read_csv(csv_file, dtype=str, header=0)
    csv_file = csv_file.astype(
        {
            "query_length_in_file_1": "int64",
            "cluster_size_in_file_1": "int64",
            "query_length_in_file_2": "int64",
            "cluster_size_in_file_2": "int64",
        }
    )

    # Create empty supercluster dict to be returned
    dict_superclusters = dict()

    # Create empty never-clust dict to be returned
    dict_never_cluster = dict()

    # Create two dictionaries mapping query_code vs cluster for each condition
    dict_query_code_and_cluster_file_1 = dict(
        zip(
            list(csv_file["query_code_in_file_1"]),
            list(csv_file["cluster_number_in_file_1"]),
        )
    )
    dict_query_code_and_cluster_file_2 = dict(
        zip(
            list(csv_file["query_code_in_file_2"]),
            list(csv_file["cluster_number_in_file_2"]),
        )
    )

    # Create one dictionary from each condition
    strict = buildDictFromCondition_In_(1, csv_file)
    relax = buildDictFromCondition_In_(2, csv_file)

    # Create a list of ordered relax clusters to be used
    list_order_clusters = sorted([int(key) for key in relax.keys()])
    list_order_clusters = [str(key) for key in list_order_clusters]

    while (
        (len(list_order_clusters) != 0)
        & (len(relax.keys()) != 0)
        & (len(strict.keys()) != 0)
    ):
        # Take first element of relax cluster list
        cltr = list_order_clusters[0]

        set_of_relax_seqs_looked = get_corresponding_seq({cltr}, relax)

        set_of_strict_clusters_looked = get_corresponding_clusters(
            set_of_relax_seqs_looked, dict_query_code_and_cluster_file_1
        )

        set_of_strict_seqs_looked = get_corresponding_seq(
            set_of_strict_clusters_looked, strict
        )

        set_of_relax_clusters_looked = get_corresponding_clusters(
            set_of_strict_seqs_looked, dict_query_code_and_cluster_file_2
        )

        # Check if this relax cluster has size 1 and also the corresponding strict cluster
        if (
            (relax[cltr][1] == 1)
            & (len(set_of_strict_clusters_looked) == 1)
            & (strict[list(set_of_strict_clusters_looked)[0]][1] == 1)
        ):
            dict_never_cluster.update({cltr: relax[cltr]})
            # Remove cluter from relax to avoid pointless calculations
            del relax[cltr], strict[list(set_of_strict_clusters_looked)[0]]
            # Remove cluster entries from list_order_clusters
            list_order_clusters.remove(cltr)
            continue

        # Build a supercluster that is "original"
        if (
            (len(set_of_strict_clusters_looked) == 1)
            & (len(set_of_relax_clusters_looked) == 1)
            & (cltr in set_of_relax_clusters_looked)
        ):
            # Build supercluster name
            supercluster_name = build_supercluster_name({cltr}, relax)
            # Add supercluster to dictionary
            dict_superclusters.update({supercluster_name: relax[cltr]})
            # Remove cluter from relax to avoid pointless calculations
            del relax[cltr], strict[list(set_of_strict_clusters_looked)[0]]
            # Remove cluster entries from list_order_clusters
            list_order_clusters.remove(cltr)
            continue

        # Build a supercluster that is "Modified but mantains same amount of sequences"
        if (
            (len(set_of_strict_clusters_looked) > 1)
            & (set_of_strict_seqs_looked == set_of_relax_seqs_looked)
        ):
            # Build supercluster name
            supercluster_name = build_supercluster_name(set_of_strict_clusters_looked, strict)
            # Add supercluster to dictionary
            dict_superclusters.update({supercluster_name: relax[cltr]})
            # Remove cluter from relax to avoid pointless calculations
            del relax[cltr], strict[list(set_of_strict_clusters_looked)[0]]
            # Remove cluster entries from list_order_clusters
            list_order_clusters.remove(cltr)
            continue

        # Build a supercluster that is not "original" (could be: Modified supercluster or a New supercluster)
        while ((len(set_of_strict_clusters_looked) > 1) &
               (set_of_strict_seqs_looked != set_of_relax_seqs_looked)
        ):
            set_of_relax_seqs_looked = get_corresponding_seq(
                set_of_relax_clusters_looked, relax
            )
            set_of_strict_clusters_looked = get_corresponding_clusters(
                set_of_relax_seqs_looked, dict_query_code_and_cluster_file_1
            )
            set_of_strict_seqs_looked = get_corresponding_seq(
                set_of_strict_clusters_looked, strict
            )
            set_of_relax_clusters_looked = get_corresponding_clusters(
                set_of_strict_seqs_looked, dict_query_code_and_cluster_file_2
            )
            set_of_relax_seqs_looked = get_corresponding_seq(
                set_of_relax_clusters_looked, relax
            )
        # Build supercluster name
        supercluster_name = build_supercluster_name(set_of_relax_clusters_looked, relax)
        # Build supercluster info to dictionary
        queries_code_length = {
            seq[0]: seq[1]
            for cltr in set_of_relax_clusters_looked
            for seq in relax[cltr][0].items()
        }
        dict_superclusters.update(
            {supercluster_name: [queries_code_length, len(queries_code_length)]}
        )
        # Remove cluters from relax to avoid pointless calculations
        for cltr in set_of_relax_clusters_looked:
            del relax[cltr]
        # Remove cluters from strict to avoid pointless calculations
        for cltr in set_of_strict_clusters_looked:
            del strict[cltr]
        # Remove cluster entries from list_order_clusters
        list_order_clusters = [
            cltr
            for cltr in list_order_clusters
            if cltr not in set_of_relax_clusters_looked
        ]
        continue

    if outputDir:
        save_superclustering_results(dict_superclusters, dict_never_cluster, outputDir)
    else:
        raise NotADirectoryError('[Error] Directory not exists: {}\nTry with \'-h\''.format(outputDir))

    if report:
        total_seq_superclusters = sum(
            dict_superclusters[cltr][1] for cltr in dict_superclusters.keys()
        )
        relax = buildDictFromCondition_In_(2, csv_file)
        total_seq_relax = sum(relax[cltr][1] for cltr in relax.keys())
        seq_of_clusters_gt_1_relax = sum(
            relax[cltr][1] for cltr in relax.keys() if relax[cltr][1] > 1
        )
        print("Total number of sequences present in dataset: " + str(len(csv_file)))
        print("Total number of sequences present in relax: " + str(total_seq_relax))
        print(
            "Total number of sequences present in clusters with more than 1 sequence in relax: "
            + str(seq_of_clusters_gt_1_relax)
        )
        print(
            "Total number of sequences present in superclusters dict: "
            + str(total_seq_superclusters)
        )
        print(
            "Total number of sequences absent in superclusters dict: "
            + str(total_seq_relax - total_seq_superclusters)
        )
        print(
            "Total number of clusters in superclusters dict: "
            + str(len(dict_superclusters))
        )
        print(
            "Total number of clusters in never cluster dict: "
            + str(len(dict_never_cluster))
        )
        print("Superclusters description:")
        for char in ["O", "M", "N"]:
            print(
                char
                + ": "
                + str(len(get_list_superclusters_type(dict_superclusters, char)))
            )

    return


if __name__ == "__main__":

    p = argparse.ArgumentParser(
        description="Generate superclusters from 2 clustering conditions."
    )
    # Positional arguments
    p.add_argument(
        "csv_file",
        help="Table .csv formated to run Superclustering algorithm"
    )
    p.add_argument(
        "outputDir",
        help="Path to output files."
    )
    # Optional arguments
    p.add_argument(
        "-r",
        "--report",
        action="store_true",
        help="Print superclustering report on screen."
    )

    args = p.parse_args()
    build_superclusters(**vars(args))

# Written by Martín González Buitrón
# Email: martingonzalezbuitron@gmail.com