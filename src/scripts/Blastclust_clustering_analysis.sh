#!/bin/bash

##############################################
#                                            #
#         Run and handle Blastclust          #
#          for clustering analisys           #
#                                            #
#      Author: Martín González Buitrón       #
#   email: martingonzalezbuitron@gmail.com   #
#                                            #
#       Universidad Nacional de Quilmes      #
#                                            #
##############################################

# Purpose: This script run and handle Blastclust running configuration
#          for nucleotide sequences. It will create many files as many
#          IDE values and COV were given.
#
#          Files created:
#                       # As many (IDE x COV) were given

# Preconditions: First line of fasta file need to begin with ">".
#                Each line of IDENTITY file need to be a float between 3 - 100
#                Each line of COVERAGE file need to be a float between 0.0 - 1.0
#                CONFIG file has one line.
#
#                IMPORTANT: Check Blastclust binary.
#                           To use this script with proteins, change -W argument to 3.

# Positional Parameters:
#         MANDATORY
#         1- FASTA file      // file (first line need to begin with ">")
#         2- IDENTITY file   // file (each line with float between 3 - 100)
#         3- COVERAGE file   // file (each line with float between 0.0 - 1.0)
#         4- CONFIG file     // file (one line with blastclust configurations.)"
#
#         OPTIONAL
#         5- OUTPUT filename + [TAG: IDE_COV] values  // file (Default: in current execution directory)

# Observation: If you want improve it, feel free to make changes. FOSS!

#Stop if any error happend
set -e

#set -x

##########################################################################################
#Clustering secuencial:
#%IDE: 100, 99, 98, 95, y 90.
#% coverage: 98, 95 y 90.
##########################################################################################

########################################################################

blastclust_binary=$(which blastclust)

#Taken from http://www.bioinformatics.org/pipermail/bbb/2006-February/003014.html
BLASTDB=/opt/blast-2.2.26/data/
BLASTMAT=/opt/blast-2.2.26/data/
export BLASTDB BLASTMAT

########################################################################

#Cheking first argument
if [[ -f "${1}" && "${1}" != "--help" && "${1}" != "-help" && "${1}" != "-h" && "${1}" != "--h" ]]
then
    fasta_file="${1}"
else
    echo
    echo " Usage:"
    echo " ====="
    echo "      ./<script.sh> <FASTA-FILE> <IDENTITY-FILE> <COVERAGE-FILE> [OUTPUT-FILE]"
    echo
    echo
    echo " Purpose: This script will handle Blastclust running configuration for nucleotide"
    echo "          sequences. It will create many files as many IDE values and COV were given."
    echo "          It use 8 cores to run."
    echo
    echo "          Files created:"
    echo "          ============="
    echo "                       # As many (IDE x COV) were given"
    echo
    echo
    echo
    echo
    echo " Preconditions: First line of fasta file need to begin with \">\"."
    echo " =============  Each line of IDENTITY file need to be a float between 3 - 100"
    echo "                Each line of COVERAGE file need to be a float between 0.0 to 1.0"
    echo "                CONFIG file has one line."
    echo
    echo "                IMPORTANT: Check Blastclust binary."
    echo "                           To use this script with proteins, change -W argument to 3."
    echo
    echo
    echo " Positional Parameters:"
    echo " ====================="
    echo
    echo "         MANDATORY"
    echo "         =========="
    echo "         1- FASTA file      // file (first line need to begin with \">\")"
    echo "         2- IDENTITY file   // file (each line with float between 3 - 100)"
    echo "         3- COVERAGE file   // file (each line with float between 0.0 to 1.0)"
    echo "         4- CONFIG file     // file (one line with blastclust configurations.)"
    echo
    echo "         OPTIONAL"
    echo "         ========="
    echo "         5- OUTPUT filename + [TAG: IDE_COV] values  // file (Default: in current execution directory)"
    echo
    echo
    exit
fi

#Cheking second argument
if [[ -f "${2}" ]]
then
    list_ide_file="${2}"
else
    if [[ -n "${2}" || -d "${2}" ]]
    then
        echo
        echo "Create a file listing all IDE values."
        echo
        echo "Use \"--help\" to see more detail info."
        exit
    else
        echo
        echo "Create a file listing all IDE values."
        echo
        echo "Use \"--help\" to see more detail info."
        exit
    fi
fi

#Cheking thirth argument
if [[ -f "${2}" && -f "${3}" ]]
then
    list_cov_file="${3}"
else
    if [[ -n "${3}" || -d "${3}" ]]
    then
        echo
        echo "Create a file listing all COV values."
        echo
        echo "Use \"--help\" to see more detail info."
        exit
    else
        echo
        echo "Create a file listing all COV values."
        echo
        echo "Use \"--help\" to see more detail info."
        echo
        exit
    fi
fi

#Cheking fourth argument
if [[ -f "${2}" && -f "${3}" && -f "${4}" ]]
then
    config_file="${4}"
else
    if [[ -n "${4}" || -d "${4}" ]]
    then
        echo
        echo "Create a blastclust config file."
        echo
        echo "Use \"--help\" to see more detail info."
        exit
    else
        echo
        echo "Create a blastclust config file."
        echo
        echo "Use \"--help\" to see more detail info."
        echo
        exit
    fi
fi

#Cheking latest argument
if [[ -n "${5}" && ! -d "${5}" ]]
then
    output_name=$(basename "${5}")
    output_dir=$(dirname "${5}")
else
    output_name=$(date +%Y-%m-%d)"_Blastclust_output"
    output_dir=$(pwd)
fi


if [[ -n "${fasta_file}" && -f "${list_ide_file}" && -f "${list_cov_file}" ]]
then
    echo "Running ..."
    while read -r ide_line
    do
        if [[ "${ide_line}" < 3 && "${ide_line}" > 100 && -z "${ide_line}" ]]
        then
            echo "ERROR: IDE value: ${ide_line}"
            echo
            echo "Use \"--help\" or try again with a correct IDE value."
            exit
        elif [[ $(echo "${ide_line}" | cut -d '.' -f 1) == "0" ]]
        then
            ide_line=$(echo "${ide_line}" | cut -d '.' -f 2)
        elif [[ $(echo "${ide_line}" | cut -d '.' -f 1) == "1" ]]
        then
            ide_line="100"
        fi
        while read -r cov_line
        do
            if [[ $(echo "${cov_line}" | cut -d '.' -f 1) != "0" && "${cov_line}" != "1" && "${cov_line}" != "1.0" && "${cov_line}" != "1.00" ]]
            then
                echo "ERROR: COV value: ${cov_line}"
                echo
                echo "Use \"--help\" or try again with a correct COV value."
                exit
            fi
            #Checking if output_name exist
            if [[ -n "${output_name}" ]]
            then
                #Calling Blastclust
                if [[ -e "${blastclust_binary}" ]]
                then
                    #Creating output_dir
                    mkdir -p "${output_dir}"
		    #Running settings (-W 9 is due our seqs have 10nt but some of them have Inosine and
		    #blastclust doesn't count them and removed them. For example: 4JV5:Y = AUUIGAAAUC)
                    "${blastclust_binary}" -i "${fasta_file}" \
                                              -a 8 \
                                              -o "${output_dir}"/"${output_name}"_ide_"${ide_line}"_cov_"${cov_line}".clstr \
                                              -L "${cov_line}" \
                                              -S "${ide_line}" \
                                              -b T \
                                              -v "${output_dir}"/"${output_name}"_ide_"${ide_line}"_cov_"${cov_line}".output \
                                              -p F \
                                              -c "${config_file}" \
                                              -W 9
                else
                    echo "Check that you have installed Blastclust binary."
                    exit
                fi
            fi
        done < "${list_cov_file}"
    done < "${list_ide_file}"
    echo "Finished !"
fi
