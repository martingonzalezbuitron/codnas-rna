#!/usr/bin/awk -f

##############################################
#                                            #
#     This script extract blast number of    #
#   each sequence that belong to a cluster   #
#   reported in blastclust *.clstr file and  #
#   create a CSV file per cluster with all   #
#              that information.             #
#                                            #
#       Also generate a CSV table with       #
#       the number of clusters of each       #
#           blastclust *.clstr file.         #
#                                            #
#                                            #
#   - Input file or files                    #
#           * blastclust *.clstr file        #
#                                            #
#   - Output dir                             #
#           * Default: results saved in      #
#             current directory under        #
#            "blastclust_clustr_info" folder.# 
#           * When an OUTPUT-FOLDER is given #
#             in last argument, results are  #
#             save there.                    #
#                                            #
#   - Stand. output per cluster (.CSV)       #
#           * HEADER:                        #
#             Seq_code                       #
#           * Content:                       #
#             <BLAST-SEQ-NUMBER>             #
#             <BLAST-SEQ-NUMBER>             #
#             <BLAST-SEQ-NUMBER>             #
#                                            #
#                                            #
#   - Standard output for blastclust run     #
#           * HEADER:                        #
#             File_name,Clusters_gt_1seq,\   #
#             Total_clusters                 #
#           * Content:                       #
#             <INPUT-FILE-1>,<NUM>,<NUM>     #
#             <INPUT-FILE-2>,<NUM>,<NUM>     #
#             <INPUT-FILE-3>,<NUM>,<NUM>     #
#             <INPUT-FILE-4>,<NUM>,<NUM>     #
#                                            #
#                                            #
#      Author: Martín González Buitrón       #
#   email: martingonzalezbuitron@gmail.com   #
#                                            #
#       Universidad Nacional de Quilmes      #
#                                            #
##############################################

# Purpose: This script extract each number corresponded to one blast sequence code
#          that was created while blastclust runs and create a CSV file per cluster
#          with all that information. Also generate a CSV table listening the name of
#          the blastclust <INPUT-FILE>, the number of clusters with more than 1 sequence
#          and the total amount of clusters.
#          You can pass multiple *.clstr files. The last argument it is the
#          <OUTPUT-FOLDER> and it is optional.
#
#       Important: The standard output is printed on screen but could be
#                  redirected to a regular file.
#

# Preconditions: 
#            * blastclust <INPUT-FILE> must end in ".clstr"

# Positional and Mandatory Parameters:
# ===================================
#   1   - blastclust *.clstr file/s         // file
#
# Positional and Optional Parameters:
# ===================================
# @LAST - <OUTPUT-FOLDER>              // string (Default: ./blastclust_clustr_info)


# Observation: If you want to improve it, feel free to make changes. FOSS!

BEGIN{
    if(ARGC <= 1 || ARGV[1] == "help" || ARGV[1] == "h" || ARGV[1] == "--SOS"){
        print "\nUsage:\n\n ./script.awk <blastclust.clstr> [<OUTPUT-FOLDER>]\n"
        print "\nFor more information run:\n"
        print " head -78 ./script.awk\n"
        print "The last argument is optional and could be an OUTPUT-FOLDER.\n"
        exit
    }
    #Build output dir
    if(ARGC > 2){
        #Getting total length of output folder
        length_dir=length(ARGV[ARGC-1])
        if(substr(ARGV[ARGC-1],(length_dir - 5),6) == ".clstr"){
            #Create output directory
            output_dir=ENVIRON["PWD"]"/""blastclust_clustr_info"
        }
        if(substr(ARGV[ARGC-1],length_dir,1) == "/"){
            last_level=split(ARGV[ARGC-1], a, "/")
            output_dir=""
            for(i=1;i<(last_level-1);i++){
                output_dir= output_dir a[i] "/"
            }
            output_dir= output_dir a[last_level-1]
        }
        if((substr(ARGV[ARGC-1],(length_dir - 5),6) != ".clstr") && (substr(ARGV[ARGC-1],length_dir,1) != "/")){
            last_level=split(ARGV[ARGC-1], a, "/")
            output_dir=""
            for(i=1;i<last_level;i++){
                output_dir= output_dir a[i] "/"
            }
            output_dir= output_dir a[last_level]
        }
    }
    else{
        #Create output directory
        output_dir=ENVIRON["PWD"]"/""blastclust_clustr_info"
    }
    #Build command to be used in system
    command="mkdir -p " output_dir
    #Create output directory
    system(command)
    #Table CSV with number of clusters gt 2 members per blastclust running configuration
    CSV_file="table_num_clusters_gt_1_seq_per_blastclust_config.csv"
    #Print header of CSV_file
    printf("File_name,Clusters_gt_1seq,Total_clusters\n") > output_dir "/" CSV_file
}
BEGINFILE{
    #Finish when you are in last argument
    if(FILENAME == ARGV[ARGC-1] && substr(ARGV[ARGC-1],length_dir - 5,6) != ".clstr"){
        exit
    }
    #Setting FILE conditions
    #Get exact file name
    file_name=split(FILENAME, a, "/")
    filename=a[file_name]
    #Build command to be used in system
    command="mkdir -p " output_dir "/" filename
    #Create output directory
    system(command)
    #Set cluster counter in zero
    count=0
    #Start to count from zero each file
    NR=0
}
{
    #Start reading RECORDS per FILE
    #Only count those cluster with more than 1 sequence
    if(NF > 1){
        cluster_number=NR-1
        cluster_name="Cluster_"cluster_number".csv"
        #Print header of this cluster
        printf("Seq_code,Length\n") > output_dir "/" filename "/" cluster_name
        for(i=1;i<=NF;i++){
            member=$i
            split(member,member_array,"_")
            printf("%s\n", member_array[2]) > output_dir "/" filename "/" cluster_name
        }
        #Count cluster entry
        count++
    }
    next
}
ENDFILE{
    #Print output per file
    printf("%s,%s,%s\n", filename,count,FNR) > output_dir "/" CSV_file
}
# Written by Martín González Buitrón
# Email: martingonzalezbuitron@gmail.com