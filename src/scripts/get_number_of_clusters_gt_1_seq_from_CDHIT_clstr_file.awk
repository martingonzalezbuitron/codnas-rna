#!/usr/bin/awk -f

##############################################
#                                            #
#      This script generate a CSV table      #
#       with the number of clusters of       #
#            a CDHit *.clstr file.           #
#                                            #
#   - Input file or files                    #
#           * CDHit *.clstr file             #
#                                            #
#                                            #
#   - Standard output for CDHit run (.CSV)   #
#           * HEADER:                        #
#             File_name,Clusters_gt_1seq,\   #
#             Total_clusters                 #
#           * Content:                       #
#             <INPUT-FILE-1>,<NUM>,<NUM>     #
#             <INPUT-FILE-2>,<NUM>,<NUM>     #
#             <INPUT-FILE-3>,<NUM>,<NUM>     #
#             <INPUT-FILE-4>,<NUM>,<NUM>     #
#                                            #
#                                            #
#      Author: Martín González Buitrón       #
#   email: martingonzalezbuitron@gmail.com   #
#                                            #
#       Universidad Nacional de Quilmes      #
#                                            #
##############################################

# Purpose: This script create a CSV table file listening the name of the
#          CDHit <INPUT-FILE> (*.clstr) and the number of clusters with
#          more than 1 member. You can pass multiple *.clstr files.
#
#       Important: The standard output is printed on screen but could be
#                  redirected to a regular file.
#

# Preconditions: 
#            * CDHit <INPUT-FILE> (*.clstr)


# Positional and Mandatory Parameters:
# ===================================
# 1 - CDHit *.clstr file/s         // file


# Observation: If you want improve it, feel free to make changes. FOSS!

BEGIN{
    #CDHit use ">Cluster" for each cluster
    #so it was chose for Record Separator
    #because of that.
    RS=">Cluster"
    #CDHit separate each seq member with a number
    #plus "\t".
    FS="\t"
    #Set counter in zero
    count=0
	#Print header of output
    printf("File_name,Clusters_gt_1seq,Total_clusters\n")
}
{
    #Only count those cluster with more than 1 conformers
    if(NF > 2){
        #Count entries
        count++
    }
}
ENDFILE{
    #Get exact file name
    file_name=split(FILENAME, a, "/")
    filename=a[file_name]
    #Print output per file
    printf("%s,%s,%s\n", filename,count,FNR-1)
    #Reboot counter
    count=0
}

# Written by Martín González Buitrón
# Email: martingonzalezbuitron@gmail.com